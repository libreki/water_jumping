<!--
SPDX-FileCopyrightText: 2022-2023 libreki

SPDX-License-Identifier: CC-BY-SA-4.0
-->

> This file is licensed under the Creative Commons Attribution-ShareAlike 4.0
> International License.

## Building

### For GNU/Linux

#### Requirements

- [CSFML](https://www.sfml-dev.org/download/csfml/)
- [GLib](https://docs.gtk.org/glib/)
- [libxml2](https://gitlab.gnome.org/GNOME/libxml2)

#### Compiling

```
$ git clone https://codeberg.org/libreki/water_jumping.git
$ cd water_jumping
$ make
```

To run the program, use:

```
$ make run
```

## License

This program is licensed under the GNU General Public License version 3 or any
later version.

### Libraries

- **CSFML** is licensed under the zlib License.
- **GLib** is licensed under the GNU Lesser General Public License version 2.1
  or any later version.
- **libxml2** is licensed under the Expat License.
