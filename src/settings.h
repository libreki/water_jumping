/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdbool.h>

#include <SDL.h>

void        settings_global_init       (void);
void        settings_global_destroy    (void);

void        settings_reset             (void);

SDL_Keycode settings_get_bind          (const char *control);

void        settings_begin_bind_change (const char *control);
void        settings_enter_bind        (SDL_Keycode key);
bool        settings_waiting_for_bind  (void);

#endif
