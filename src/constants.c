/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "constants.h"

#include <glib.h>

#include "util/xml.h"

static GHashTable *constants;

void
constants_init (void)
{
  xmlDoc *doc;
  xmlNode *node;

  constants = g_hash_table_new_full (g_str_hash, g_str_equal, free, free);

  doc = xml_read_file_or_exit ("./data/config/constants.xml");
  node = xmlDocGetRootElement (doc);
  xml_assert_element (node, "constants");

  node = node->children;
  while (true)
    {
      xml_assert_element (node, "constant");

      g_hash_table_insert (constants,
                           xml_get_prop_string (node, "name"),
                           xml_get_prop_string (node, "value"));

      XML_NEXT_OR_BREAK (node);
    }

  xmlFreeDoc (doc);
}

void
constants_destroy (void)
{
  g_hash_table_destroy (constants);
}

const char *
constant_get (const char *name)
{
  return g_hash_table_lookup (constants, name);
}
