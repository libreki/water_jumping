/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <SDL_image.h>
#include <SDL_ttf.h>

#include "constants.h"
#include "editor/editor-renderer.h"
#include "events.h"
#include "level.h"
#include "menu/menu.h"
#include "settings.h"
#include "system/systems.h"
#include "util/input.h"
#include "video/graphics.h"

static void
main_init (SDL_Window   **window,
           SDL_Renderer **renderer,
           editor        *e,
           Input         *input)
{
  if (SDL_Init (SDL_INIT_VIDEO) != 0)
    {
      SDL_Log ("SDL_Init error: %s", SDL_GetError ());
      SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR,
                                "SDL_Init error",
                                SDL_GetError (), NULL);

      exit (1);
    }

  *window = SDL_CreateWindow ("water_jumping",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              WINDOW_WIDTH, WINDOW_HEIGHT,
                              SDL_WINDOW_SHOWN);
  if (!*window)
    {
      SDL_Log ("SDL_CreateWindow error: %s", SDL_GetError ());
      SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR,
                                "SDL_CreateWindow error",
                                SDL_GetError (), NULL);

      SDL_Quit ();
      exit (1);
    }

  *renderer = SDL_CreateRenderer (*window, -1, 0);
  if (!*renderer)
    {
      SDL_Log ("SDL_CreateRenderer error: %s", SDL_GetError ());
      SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR,
                                "SDL_CreateRenderer error",
                                SDL_GetError (), NULL);

      SDL_DestroyWindow (*window);
      SDL_Quit ();
      exit (1);
    }

  SDL_SetHint (SDL_HINT_RENDER_SCALE_QUALITY, "linear");

  if ((IMG_Init (IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
    {
      SDL_Log ("IMG_Init error: %s", SDL_GetError ());
      SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR,
                                "IMG_Init error",
                                SDL_GetError (), NULL);

      SDL_DestroyRenderer (*renderer);
      SDL_DestroyWindow (*window);
      SDL_Quit ();
      exit (1);
    }

  if (TTF_Init () != 0)
    {
      SDL_Log ("TTF_Init error: %s", SDL_GetError ());
      SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR,
                                "TTF_Init error",
                                SDL_GetError (), NULL);

      IMG_Quit ();
      SDL_DestroyRenderer (*renderer);
      SDL_DestroyWindow (*window);
      SDL_Quit ();
      exit (1);
    }

  xmlKeepBlanksDefault (0);

  constants_init ();
  settings_global_init ();
  ecs_global_init ();
  editor_init (e);
  graphics_global_init (*renderer);
  menu_global_init ();

  input->previous.up = false;
  input->previous.left = false;
  input->previous.down = false;
  input->previous.right = false;
  input->previous.jump = false;
}

static void
main_destroy (SDL_Window   **window,
              SDL_Renderer **renderer,
              level         *lvl)
{
  level_destroy (lvl); /* TODO: Segfault if level hasn't been loaded */
  graphics_global_destroy ();
  menu_global_destroy ();
  ecs_global_destroy ();
  settings_global_destroy ();
  constants_destroy ();

  TTF_Quit ();
  IMG_Quit ();
  SDL_DestroyRenderer (*renderer);
  SDL_DestroyWindow (*window);
  SDL_Quit ();
}

int
main (void)
{
  SDL_Window *window;
  SDL_Renderer *renderer;
  Camera camera;
  Input input;
  level lvl;
  editor e;
  bool running;

  main_init (&window, &renderer, &e, &input);

  running = true;
  while (running)
    {
      ecs_t *ecs;

      poll_events (&running, &e, &lvl, &camera);
      input_update (&input);

      ecs = level_current_ecs (&lvl);

      SDL_SetRenderDrawColor (renderer, 0, 0, 0, 255);
      SDL_RenderClear (renderer);

      if (editor_text_entry_active (&e))
        render_text_entry (renderer, &e.te);
      else if (!menu_is_open () || menu_editor_menu_is_open ())
        {
          puffer_system_update (ecs, !e.editing);
          puffer_system_update_sprite_only (ecs, e.editing);
          player_system_update (ecs, input, &lvl, !e.editing);
          checkpoint_system_update (ecs, &lvl, !e.editing && !e.active);
          physics_system_update (ecs, !e.editing);
          scene_warp_system_update (ecs, &lvl, !e.editing);

          camera_system_update (ecs, &camera, !e.editing);
          render_system_update (ecs, renderer, &camera, &e, true);

          level_update (&lvl);

          if (e.active)
            {
              render_editor_hud (ecs, renderer, &e);

              if (menu_is_open ())
                render_menu (renderer);
            }
        }
      else if (menu_is_open ())
        render_menu (renderer);

      game_time_update ();

      SDL_RenderPresent (renderer);
      SDL_Delay (16);
    }

  main_destroy (&window, &renderer, &lvl);

  return 0;
}
