/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef LEVEL_H
#define LEVEL_H

#include "ecs/ecs.h"

typedef struct scene
{
  ecs_t ecs;
} scene;

typedef struct level_save
{
  scene *scenes;
  int    current_scene;
} level_save;

typedef struct level
{
  scene      *scenes;
  int         scenes_length;
  int         current_scene;

  level_save  save_state;
  bool        load_checkpoint;

  bool        exit;
} level;

/* Loads a level from an XML file in ./data/level/lvl_name.xml or creates a new
   empty level if a level with name lvl_name can't be opened. */
void   level_init               (level      *lvl,
                                 const char *lvl_name);
void   level_destroy            (level *lvl);

void   level_scene_new          (level *lvl);

void   level_update             (level *lvl);

ecs_t *level_current_ecs        (level *lvl);

void   level_reset_entities     (level *lvl);
void   level_save_current_state (level *lvl);

void   level_xml_write          (level      *lvl,
                                 const char *lvl_name);

#endif
