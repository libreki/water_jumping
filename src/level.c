/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "level.h"

#include <glib.h>

#include "ecs/ecs.h"
#include "menu/menu.h"
#include "util/entity.h"
#include "util/spawn-state.h"
#include "util/xml.h"
#include "video/tilemap.h"

/* Loads a scene from a scene XML node pointed to by node, or creates an empty
   scene if node is NULL. */
static void
scene_init (scene   *s,
            xmlNode *node)
{
  ecs_init (&s->ecs);

  if (!node)
    {
      ecs_entity_of_type_new (&s->ecs, "player", NULL);
      return;
    }

  node = node->children;
  while (true)
    {
      xml_assert_element (node, "entity");

      ecs_entity_new_from_xml (&s->ecs, node);

      XML_NEXT_OR_BREAK (node);
    }
  node = node->parent;
}

static void
scene_destroy (scene *s)
{
  ecs_destroy (&s->ecs);
}

static void
scenes_destroy (scene *scenes,
                int    scenes_length,
                bool   finalize)
{
  for (int i = 0; i < scenes_length; i++)
    scene_destroy (&scenes[i]);

  if (finalize)
    free (scenes);
}

void
level_init (level      *lvl,
            const char *lvl_name)
{
  xmlDoc *doc;
  xmlNode *node;
  char *filename;

  lvl->current_scene = 0;
  lvl->save_state.scenes = NULL;
  lvl->save_state.current_scene = -1;
  lvl->load_checkpoint = false;
  lvl->exit = false;

  filename = g_strdup_printf ("./data/level/%s.xml", lvl_name);
  doc = xmlReadFile (filename, NULL, 0);
  if (doc)
    {
      int i;

      node = xmlDocGetRootElement (doc);

      xml_assert_element (node, "level");

      lvl->scenes = malloc (xmlChildElementCount (node) * sizeof (scene));
      lvl->scenes_length = xmlChildElementCount (node);

      i = 0;
      node = node->children;
      while (true)
        {
          xml_assert_element (node, "scene");

          scene_init (&lvl->scenes[i], node);
          i++;

          XML_NEXT_OR_BREAK (node);
        }

      xmlFreeDoc (doc);
    }
  else
    {
      lvl->scenes = malloc (1 * sizeof (scene));
      lvl->scenes_length = 1;
      scene_init (&lvl->scenes[0], NULL);
    }

  free (filename);

  for (int j = 0; j < lvl->scenes_length; j++)
    {
      tilemap_generate_tiles (&lvl->scenes[j].ecs);
      puffer_adjust_starting_velocity (&lvl->scenes[j].ecs);
      spawn_state_save_entities (&lvl->scenes[j].ecs);
    }

  level_save_current_state (lvl);
}

void
level_destroy (level *lvl)
{
  scenes_destroy (lvl->scenes, lvl->scenes_length, true);
  scenes_destroy (lvl->save_state.scenes, lvl->scenes_length, true);
}

void
level_scene_new (level *lvl)
{
  lvl->scenes_length++;
  lvl->scenes = realloc (lvl->scenes, lvl->scenes_length * sizeof (scene));
  lvl->save_state.scenes = realloc (lvl->save_state.scenes,
                                    lvl->scenes_length * sizeof (scene));
}

void
level_update (level *lvl)
{
  if (lvl->exit)
    {
      menu_open (OVERWORLD_T);
      level_destroy (lvl);
    }
  else if (lvl->load_checkpoint)
    {
      scenes_destroy (lvl->scenes, lvl->scenes_length, false);

      lvl->current_scene = lvl->save_state.current_scene;
      for (int i = 0; i < lvl->scenes_length; i++)
        ecs_copy (&lvl->save_state.scenes[i].ecs, &lvl->scenes[i].ecs);

      lvl->load_checkpoint = false;
    }
}

ecs_t *
level_current_ecs (level *lvl)
{
  return &lvl->scenes[lvl->current_scene].ecs;
}

void
level_reset_entities (level *lvl)
{
  for (int i = 0; i < lvl->scenes_length; i++)
    spawn_state_reset_entities (&lvl->scenes[i].ecs);
}

void
level_save_current_state (level *lvl)
{
  if (lvl->save_state.scenes)
    scenes_destroy (lvl->save_state.scenes, lvl->scenes_length, false);
  else
    lvl->save_state.scenes = malloc (lvl->scenes_length * sizeof (scene));

  lvl->save_state.current_scene = lvl->current_scene;
  for (int i = 0; i < lvl->scenes_length; i++)
    {
      ecs_copy (&lvl->scenes[i].ecs, &lvl->save_state.scenes[i].ecs);
      spawn_state_reset_entities (&lvl->save_state.scenes[i].ecs);
    }
}

void
level_xml_write (level      *lvl,
                 const char *lvl_name)
{
  xmlTextWriter *writer;
  char *filename;

  filename = g_strdup_printf ("./data/level/%s.xml", lvl_name);
  writer = xml_new_writer (filename);
  free (filename);

  xmlTextWriterWriteComment (writer, (xmlChar *) "\n  License\n");

  xml_start_element (writer, "level");

  for (int i = 0; i < lvl->scenes_length; i++)
    {
      scene *s;

      xml_start_element (writer, "scene");

      xml_write_attribute_int (writer, "index", i);
      s = &lvl->scenes[i];
      for (int entity = 0; entity < s->ecs.entity_count; entity++)
        {
          xml_start_element (writer, "entity");

          ecs_entity_xml_write (&s->ecs, entity, writer);

          xml_end_element (writer);
        }

      xml_end_element (writer);
    }

  xml_end_element (writer);

  xmlTextWriterEndDocument (writer);
  xmlFreeTextWriter (writer);
}
