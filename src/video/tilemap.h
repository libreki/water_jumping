/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TILESET_H
#define TILESET_H

#include <SDL.h>

#include "ecs/ecs.h"
#include "sprite.h"

typedef struct
{
  char   *tileset;
  Sprite *tiles;
  int     tiles_length;
} Tilemap;

void tilemap_ptr_copy       (component_t *src,
                             component_t *dest);
void tilemap_ptr_destroy    (component_t *component);

void tilemap_generate_tiles (ecs_t *ecs);

#endif
