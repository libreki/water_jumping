/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "graphics.h"

#include <glib.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "sprite.h"
#include "util/xml.h"

/* NULL-terminated array of all loaded texture atlases. Texture and Animation
 * objects hold const pointers to values stored in this array. */
static SDL_Texture **atlases = NULL;

/* Keys are strings, values are Texture pointers. */
static GHashTable *textures = NULL;

/* Keys are strings, values are Animation pointers. */
static GHashTable *animations = NULL;

/* Creates a Texture object and stores it in the textures hash table. */
static void
texture_load (const SDL_Texture *atlas,
              xmlNode           *node,
              char              *name)
{
  Texture *texture;

  texture = malloc (sizeof (Texture));
  texture->atlas = atlas;
  texture->src.x = xml_get_prop_int (node, "x");
  texture->src.y = xml_get_prop_int (node, "y");
  texture->src.w = xml_get_prop_int (node, "width");
  texture->src.h = xml_get_prop_int (node, "height");

  g_hash_table_insert (textures, name, texture);
}

/* Creates an Animation object and stores it in the animations hash table. */
static void
animation_load (const SDL_Texture *atlas,
                xmlNode           *node,
                char              *name)
{
  g_hash_table_insert (animations, name,
                       animation_new_from_xml (atlas, node));
}

static void
tileset_load (const SDL_Texture *atlas,
              xmlNode           *node)
{
  char *tileset_name;

  tileset_name = xml_get_prop_string (node, "name");

  node = node->children;
  while (true)
    {
      char *tile_name, *name;

      tile_name = xml_get_prop_string (node, "name");
      name = g_strdup_printf ("%s_%s", tileset_name, tile_name);

      if (xml_element_is (node, "texture"))
        texture_load (atlas, node, name);
      else if (xml_element_is (node, "animation"))
        animation_load (atlas, node, name);

      free (tile_name);

      XML_NEXT_OR_BREAK (node);
    }
  node = node->parent;

  free (tileset_name);
}

static void
texture_atlas_load (SDL_Renderer *renderer,
                    const char   *name)
{
  char *image_file, *xml_file;
  SDL_Texture *atlas;
  int null_index;
  xmlDoc *doc;
  xmlNode *node;

  image_file = g_strdup_printf ("./data/image/%s.png", name);
  xml_file = g_strdup_printf ("./data/image/%s.xml", name);

  atlas = IMG_LoadTexture (renderer, image_file);
  free (image_file);

  /* Find the index of the terminating NULL in atlases. */
  null_index = 0;
  while (atlases[null_index])
    null_index++;

  /* Grow atlases array and add newly loaded texture atlas into it. */
  atlases = realloc (atlases, (null_index + 2) * sizeof (SDL_Texture *));
  atlases[null_index] = atlas;
  atlases[null_index + 1] = NULL;

  doc = xml_read_file_or_exit (xml_file);
  node = xmlDocGetRootElement (doc);
  xml_assert_element (node, "image");
  free (xml_file);

  node = node->children;
  while (true)
    {
      if (xml_element_is (node, "texture"))
        texture_load (atlas, node, xml_get_prop_string (node, "name"));
      if (xml_element_is (node, "animation"))
        animation_load (atlas, node, xml_get_prop_string (node, "name"));
      if (xml_element_is (node, "tileset"))
        tileset_load (atlas, node);

      XML_NEXT_OR_BREAK (node);
    }

  xmlFreeDoc (doc);
}

void
graphics_global_init (SDL_Renderer *renderer)
{
  atlases = malloc (sizeof (SDL_Texture *));
  atlases[0] = NULL;

  textures =
    g_hash_table_new_full (g_str_hash, g_str_equal,
                           free, free);

  animations =
    g_hash_table_new_full (g_str_hash, g_str_equal,
                           free, (void (*) (void *)) animation_free);

  texture_atlas_load (renderer, "puffer");
  texture_atlas_load (renderer, "spikes");
  texture_atlas_load (renderer, "tiles");
  texture_atlas_load (renderer, "water");
  texture_atlas_load (renderer, "gplv3");
  texture_atlas_load (renderer, "overworld_background");
  texture_atlas_load (renderer, "overworld_level_icons");
}

void
graphics_global_destroy (void)
{
  for (int i = 0; atlases[i]; i++)
    SDL_DestroyTexture (atlases[i]);

  free (atlases);
  g_hash_table_destroy (textures);
  g_hash_table_destroy (animations);
}

const Texture *
graphics_get_texture (const char *name)
{
  return g_hash_table_lookup (textures, name);
}

const Animation *
graphics_get_animation (const char *name)
{
  return g_hash_table_lookup (animations, name);
}
