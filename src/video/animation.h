/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ANIMATION_H
#define ANIMATION_H

#include <SDL.h>

#include "util/timer.h"
#include "util/xml.h"

typedef struct
{
  /* The source position of this frame in the animation texture atlas. */
  SDL_Rect src;

  /* The amount of ticks this frame will be shown for. */
  int      time;
} Frame;

typedef struct
{
  /* Texture atlas containing all frames of the animation. */
  const SDL_Texture *atlas;

  /* Dynamically allocated array of all frame objects of this animation. */
  Frame             *frames;
  int                frame_count;

  /* The length of the animation in ticks. */
  int                time;

  /* Is the animation supposed to loop (true) or only play once (false). */
  bool               looping;
} Animation;

Animation   *animation_new_from_xml      (const SDL_Texture *atlas,
                                          xmlNode           *node);
void         animation_free              (Animation *animation);

bool         animation_is_over           (const Animation *animation,
                                          timer           *tmr);

/* Returns NULL if animation has been fully played. */
const Frame *animation_get_current_frame (const Animation *animation,
                                          timer           *tmr);

#endif
