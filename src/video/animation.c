/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "animation.h"

static Animation *
animation_new (const SDL_Texture *atlas,
               int                frame_count,
               bool               looping)
{
  Animation *animation;

  animation = malloc (sizeof (Animation));

  animation->atlas = atlas;
  animation->frame_count = frame_count;
  animation->frames = malloc (animation->frame_count * sizeof (Frame));
  for (int i = 0; i < animation->frame_count; i++)
    {
      animation->frames[i].src = (SDL_Rect) { 0, 0, 0, 0 };
      animation->frames[i].time = 0;
    }

  animation->frame_count = frame_count;
  animation->time = 0;
  animation->looping = looping;

  return animation;
}

Animation *
animation_new_from_xml (const SDL_Texture *atlas,
                        xmlNode           *node)
{
  Animation *animation;
  int i;

  animation = animation_new (atlas, xmlChildElementCount (node),
                             xml_get_prop_bool (node, "looping"));

  i = 0;
  node = node->children;
  while (true)
    {
      xml_assert_element (node, "frame");

      animation->frames[i].src.x = xml_get_prop_int (node, "x");
      animation->frames[i].src.y = xml_get_prop_int (node, "y");
      animation->frames[i].src.w = xml_get_prop_int (node, "width");
      animation->frames[i].src.h = xml_get_prop_int (node, "height");
      animation->frames[i].time = xml_get_prop_int (node, "time");

      animation->time += animation->frames[i].time;

      i++;
      XML_NEXT_OR_BREAK (node);
    }
  node = node->parent;

  return animation;
}

void
animation_free (Animation *animation)
{
  free (animation->frames);
  free (animation);
}

bool
animation_is_over (const Animation *animation,
                   timer           *tmr)
{
  if (animation->looping)
    return false;

  return timer_elapsed_ticks (tmr) >= animation->time;
}

const Frame *
animation_get_current_frame (const Animation *animation,
                             timer           *tmr)
{
  int time;

  time = timer_elapsed_ticks (tmr);
  if (animation->looping && time >= animation->time)
    time %= animation->time;

  for (int i = 0; i < animation->frame_count; i++)
    {
      time -= animation->frames[i].time;
      if (time <= 0)
        return &animation->frames[i];
    }

  return NULL;
}
