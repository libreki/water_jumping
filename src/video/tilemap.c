/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tilemap.h"

#include <glib.h>

#include "constants.h"
#include "graphics.h"
#include "util/entity.h"
#include "util/xml.h"

void
tilemap_ptr_copy (component_t *src,
                  component_t *dest)
{
  Tilemap *tilemap_src, *tilemap_dest;
  int tiles_size;

  tilemap_src = src->ptr;
  tilemap_dest = dest->ptr;
  tiles_size = tilemap_src->tiles_length * sizeof (Sprite);

  tilemap_dest->tileset = g_strdup (tilemap_src->tileset);
  tilemap_dest->tiles = malloc (tiles_size);
  tilemap_dest->tiles_length = tilemap_src->tiles_length;

  for (int i = 0; i < tilemap_dest->tiles_length; i++)
    tilemap_dest->tiles[i] = tilemap_src->tiles[i];
}

void
tilemap_ptr_destroy (component_t *component)
{
  Tilemap *tilemap;

  tilemap = component->ptr;
  free (tilemap->tileset);
  free (tilemap->tiles);

  free (component->ptr);
}

enum Surroundings
{
  TOP_LEFT = 0,
  TOP = 1,
  TOP_RIGHT = 2,
  LEFT = 3,
  RIGHT = 5,
  BOTTOM_LEFT = 6,
  BOTTOM = 7,
  BOTTOM_RIGHT = 8
};

static void
tilemap_construct_tile_at_pos (ecs_t        *ecs,
                               int          *potential_neighbors,
                               const char   *tileset,
                               Sprite       *tile,
                               double        x,
                               double        y)
{
  bool empty[9];
  int i;
  const char *tile_name;
  char *name;
  const Texture *texture;
  const Animation *animation;

  tile->dest.x = x;
  tile->dest.y = y;
  tile->dest.w = TILE_WIDTH;
  tile->dest.h = TILE_WIDTH;

  i = 0;
  for (double ys = y - TILE_WIDTH; ys <= y + TILE_WIDTH; ys += TILE_WIDTH)
    {
      for (double xs = x - TILE_WIDTH; xs <= x + TILE_WIDTH; xs += TILE_WIDTH)
        {
          empty[i] = true;
          for (int j = 0; potential_neighbors[j] >= 0; j++)
            {
              Tilemap *neighbor_tilemap;

              neighbor_tilemap =
                ecs_entity_get_component_ptr (ecs, potential_neighbors[j],
                                              "Tilemap");

              if (g_str_equal (tileset, neighbor_tilemap->tileset)
                  && rect_is_at_pos (ecs, potential_neighbors[j],
                                     xs + TILE_WIDTH / 2,
                                     ys + TILE_WIDTH / 2))
                {
                  empty[i] = false;
                }
            }
          i++;
        }
    }

  if (empty[TOP])
    {
      if (empty[RIGHT])
        tile_name = "tile_top_right";
      else if (empty[LEFT])
        tile_name = "tile_top_left";
      else
        tile_name = "tile_top";
    }
  else if (empty[BOTTOM])
    {
      if (empty[RIGHT])
        tile_name = "tile_bottom_right";
      else if (empty[LEFT])
        tile_name = "tile_bottom_left";
      else
        tile_name = "tile_bottom";
    }
  else if (empty[RIGHT])
    tile_name = "tile_right";
  else if (empty[LEFT])
    tile_name = "tile_left";
  else if (empty[TOP_LEFT])
    tile_name = "tile_in_bottom_right";
  else if (empty[TOP_RIGHT])
    tile_name = "tile_in_bottom_left";
  else if (empty[BOTTOM_LEFT])
    tile_name = "tile_in_top_right";
  else if (empty[BOTTOM_RIGHT])
    tile_name = "tile_in_top_left";
  else
    tile_name = "tile_center";

  name = g_strdup_printf ("%s_%s", tileset, tile_name);

  texture = graphics_get_texture (name);
  animation = graphics_get_animation (name);

  if (texture)
    sprite_display_texture (tile, texture);
  else if (animation)
    sprite_play_animation (tile, animation);

  free (name);
}

void
tilemap_generate_tiles (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, tilemap_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      int tile_i;
      position *pos;
      dimensions *dim;
      Tilemap *tilemap;
      double max_x, max_y;

      pos = ecs_entity_get_component_ptr (ecs, entities[i], "position");
      dim = ecs_entity_get_component_ptr (ecs, entities[i], "dimensions");
      tilemap = ecs_entity_get_component_ptr (ecs, entities[i], "Tilemap");

      if (tilemap->tiles_length > 0)
        free (tilemap->tiles);

      max_x = pos->x + dim->width - TILE_WIDTH;
      max_y = pos->y + dim->height - TILE_WIDTH;

      tilemap->tiles_length = ((dim->width / TILE_WIDTH)
                               * (dim->height / TILE_WIDTH));
      tilemap->tiles = malloc (tilemap->tiles_length * sizeof (Sprite));

      tile_i = 0;
      for (double x = pos->x; x <= max_x; x += TILE_WIDTH)
        {
          for (double y = pos->y; y <= max_y; y += TILE_WIDTH)
            {
              tilemap_construct_tile_at_pos (ecs, entities, tilemap->tileset,
                                             &tilemap->tiles[tile_i], x, y);
              tile_i++;
            }
        }
    }

  free (entities);
}
