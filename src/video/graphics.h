/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "animation.h"

typedef struct
{
  const SDL_Texture *atlas;
  SDL_Rect           src;
} Texture;

void             graphics_global_init    (SDL_Renderer *renderer);
void             graphics_global_destroy (void);

const Texture   *graphics_get_texture    (const char *name);
const Animation *graphics_get_animation  (const char *name);

#endif
