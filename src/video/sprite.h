/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SPRITE_H
#define SPRITE_H

#include <SDL.h>

#include "graphics.h"

typedef enum
{
  STATIC_SPRITE,
  ANIMATED_SPRITE
} Sprite_Type;

/* A Sprite is a renderable component which can be used to display singular
 * static textures or animations. */
typedef struct
{
  SDL_Rect         dest;
  Sprite_Type      type;

  const Texture   *texture;

  const Animation *animation;
  timer            animation_timer;
} Sprite;

void sprite_display_texture (Sprite        *sprite,
                             const Texture *texture);
void sprite_play_animation  (Sprite          *sprite,
                             const Animation *animation);

#endif
