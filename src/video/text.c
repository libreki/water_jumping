/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "text.h"

#include <glib.h>
#include <SDL_ttf.h>

void
render_text (SDL_Renderer *renderer,
             const char   *font_name,
             int           font_size,
             SDL_Color     color,
             const char   *text,
             int           x,
             int           y)
{
  TTF_Font *font;
  SDL_Surface *surface;
  SDL_Texture *texture;
  SDL_Rect dest;
  char *path;

  path = g_strdup_printf ("./data/fonts/%s", font_name);
  font = TTF_OpenFont (path, font_size);
  free (path);

  surface = TTF_RenderText_Blended (font, text, color);
  texture = SDL_CreateTextureFromSurface (renderer, surface);

  dest.x = x;
  dest.y = y;

  /* Get the width and height of the texture. */
  SDL_QueryTexture (texture, NULL, NULL, &dest.w, &dest.h);

  SDL_RenderCopy (renderer, texture, NULL, &dest);

  SDL_DestroyTexture (texture);
  SDL_FreeSurface (surface);
  TTF_CloseFont (font);
}
