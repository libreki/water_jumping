/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "editor-camera.h"

void
editor_camera_drag_start (editor *e,
                          Camera *camera,
                          double  x,
                          double  y)
{
  e->camera_start.x = camera->x;
  e->camera_start.y = camera->y;
  e->drag_start.x = x;
  e->drag_start.y = y;

  e->dragging = true;
}

void
editor_camera_drag_update (editor *e,
                           Camera *camera,
                           double  x,
                           double  y)
{
  if (!e->dragging)
    return;

  camera->x = e->camera_start.x + e->drag_start.x - x;
  camera->y = e->camera_start.y + e->drag_start.y - y;
}

void
editor_camera_drag_finish (editor *e)
{
  e->dragging = false;
}
