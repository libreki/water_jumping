/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "editor.h"

#include <assert.h>

#include "util/entity.h"
#include "util/spawn-state.h"
#include "video/tilemap.h"

void
editor_init (editor *e)
{
  for (int i = 0; i < 256; i++)
    e->lvl_name[i] = '\0';

  text_entry_init (&e->te);
  e->active = false;
  e->editing = false;
  e->selected_entity = -1;
  e->creating_entity_type = NULL;
}

void
editor_start (editor *e)
{
  editor_text_entry_activate (e, "load_level");
  e->active = true;
  e->editing = true;
}

void
editor_toggle_editing (editor *e,
                       level  *lvl)
{
  e->editing = !e->editing;
  level_reset_entities (lvl);
  tilemap_generate_tiles (level_current_ecs (lvl));
  puffer_adjust_starting_velocity (level_current_ecs (lvl));
  if (!e->editing)
    level_save_current_state (lvl);
}

void
editor_stop (editor *e)
{
  for (int i = 0; i < 256; i++)
    e->lvl_name[i] = '\0';

  e->active = false;
  e->editing = false;
}

void
editor_text_entry_activate (editor     *e,
                            const char *purpose)
{
  e->te.purpose = purpose;
}

bool
editor_text_entry_active (editor *e)
{
  return e->te.purpose != NULL;
}

void
editor_text_entry_submit (editor *e,
                          level  *lvl)
{
#define IF_PURPOSE_IS_MEMBER_FREE_MEMBER_STR(c, m)                      \
  if (g_str_equal (e->te.purpose, #m))                                  \
    {                                                                   \
      c *comp;                                                          \
                                                                        \
      comp = ecs_entity_get_component_ptr (level_current_ecs (lvl),     \
                                           e->selected_entity, #c);     \
      if (comp->m)                                                      \
        free (comp->m);                                                 \
    }

#define IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT(c, m, f)                  \
  if (g_str_equal (e->te.purpose, #m))                                  \
    {                                                                   \
      ecs_t *ecs;                                                       \
      c *comp;                                                          \
                                                                        \
      ecs = level_current_ecs (lvl);                                    \
      comp = ecs_entity_get_component_ptr (ecs, e->selected_entity, #c); \
                                                                        \
      comp->m = f (e->te.text);                                         \
                                                                        \
      if (ecs_entity_has_component (ecs, e->selected_entity, "spawn_state")) \
        spawn_state_save_entity (ecs, e->selected_entity);              \
    }

  if (g_str_equal (e->te.purpose, "load_level"))
    {
      level_init (lvl, e->te.text);
      strcpy (e->lvl_name, e->te.text);
    }
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (position, x, atof);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (position, y, atof);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (dimensions, width, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (dimensions, height, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (scene_warp, scene_index, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (scene_warp, player_x, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (scene_warp, player_y, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (puffer, periods_elapsed, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (puffer, movement_direction, atoi);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (puffer, accelerate, atoi);
  IF_PURPOSE_IS_MEMBER_FREE_MEMBER_STR (Tilemap, tileset);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (Tilemap, tileset, g_strdup);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (camera, center_x, atof);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (camera, center_y, atof);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (camera, zoom, atof);
  IF_PURPOSE_IS_MEMBER_UPDATE_COMPONENT (camera, threshold, atof);

  text_entry_clear (&e->te);
  e->te.purpose = NULL;
}
