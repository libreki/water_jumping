/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TEXT_ENTRY_H
#define TEXT_ENTRY_H

#include <stdbool.h>

#include <SDL.h>

typedef struct text_entry
{
  char        text[256];
  const char *purpose;
} text_entry;

void render_text_entry      (SDL_Renderer *renderer,
                             text_entry   *te);

void text_entry_init        (text_entry *te);

void text_entry_enter_char  (text_entry *te,
                             char        c);
void text_entry_delete_char (text_entry *te);
void text_entry_clear       (text_entry *te);

#endif
