/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef EDITOR_CAMERA_H
#define EDITOR_CAMERA_H

#include <SDL.h>

#include "editor.h"
#include "video/camera.h"

/* x and y are game coordinates translated from screen coordinates with
 * to_game_coordinate. */
void       editor_camera_drag_start  (editor *e,
                                      Camera *camera,
                                      double  x,
                                      double  y);

/* x and y are game coordinates translated from screen coordinates with
 * to_game_coordinate. */
void       editor_camera_drag_update (editor *e,
                                      Camera *camera,
                                      double  x,
                                      double  y);

void       editor_camera_drag_finish (editor *e);

#endif
