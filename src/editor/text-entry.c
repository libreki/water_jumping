/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "text-entry.h"

#include <string.h>

#include "video/graphics.h"
#include "video/text.h"

void
render_text_entry (SDL_Renderer *renderer,
                   text_entry   *te)
{
  render_text (renderer, "LiberationMono-Regular.ttf", 64,
               (SDL_Color) { 255, 255, 255, 255 }, te->text, 0, 0);
}

void
text_entry_init (text_entry *te)
{
  text_entry_clear (te);
  te->purpose = NULL;
}

void
text_entry_enter_char (text_entry *te,
                       char        c)
{
  int length;

  length = strlen (te->text);
  if (length < 255)
    {
      te->text[length] = c;
      te->text[length + 1] = '\0';
    }
}

void
text_entry_delete_char (text_entry *te)
{
  int length;

  length = strlen (te->text);
  if (length > 0)
    te->text[length - 1] = '\0';
}

void
text_entry_clear (text_entry *te)
{
  for (int i = 0; i < 256; i++)
    te->text[i] = '\0';
}
