/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "editor-renderer.h"

#include "ecs/ecs.h"
#include "video/text.h"

#define TEXT_SIZE 24

void
render_editor_hud (ecs_t        *ecs,
                   SDL_Renderer *renderer,
                   editor       *e)
{
  SDL_Color color;

  if (e->editing)
    color = (SDL_Color) { 255, 255, 0, 255 };
  else
    color = (SDL_Color) { 255, 255, 255, 255 };

  render_text (renderer, "LiberationMono-Regular.ttf",
               TEXT_SIZE, color, "EDITOR",
               0, 0);

  if (e->editing && e->creating_entity_type)
    {
      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, (SDL_Color) { 255, 255, 255, 255 },
                   e->creating_entity_type, 0, TEXT_SIZE);
    }
}
