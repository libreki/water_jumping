/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "edit-entity.h"

#include <math.h>

#include "constants.h"
#include "menu/menu.h"
#include "util/entity.h"
#include "util/input.h"
#include "util/spawn-state.h"
#include "video/tilemap.h"

static double
to_grid_coordinate (int n)
{

  return TILE_WIDTH * floor (n / TILE_WIDTH);
}

void
editor_create_entity_at_position (editor *e,
                                  ecs_t  *ecs,
                                  double  x,
                                  double  y)
{
  if (e->creating_entity_type
      && !g_str_equal (e->creating_entity_type, "player"))
    {
      e->selected_entity = ecs_entity_of_type_new (ecs,
                                                   e->creating_entity_type,
                                                   NULL);

      if (ecs_entity_has_component (ecs, e->selected_entity, "spawn_state"))
        spawn_state_save_entity (ecs, e->selected_entity);

      editor_move_selected_entity_to_position (e, ecs, x, y);
    }
}

void
editor_delete_selected_entity (editor *e,
                               ecs_t  *ecs)
{
  if (e->selected_entity < 0
      || ecs_entity_is_of_type (ecs, e->selected_entity, "player"))
    {
      return;
    }

  ecs_entity_delete (ecs, e->selected_entity);
  e->selected_entity = -1;
}

void
editor_attempt_select_entity (editor *e,
                              ecs_t  *ecs,
                              double  x,
                              double  y)
{
  e->selected_entity = rect_at_pos (ecs, x, y);

  if (menu_stack_head_is (COMPONENT_MENU_T))
    menu_close ();

  menu_update_component_menu (e, ecs);
}

static void
editor_selected_entity_update_spawn_position (editor   *e,
                                              ecs_t    *ecs,
                                              position *pos)
{
  spawn_state *ss;
  position *ss_pos;

  if (!ecs_entity_has_component (ecs, e->selected_entity, "spawn_state"))
    return;

  ss = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                     "spawn_state");
  ss_pos = spawn_state_get_component_ptr (ss, "position");

  ss_pos->x = pos->x;
  ss_pos->y = pos->y;
}

void
editor_move_selected_entity (editor *e,
                             ecs_t  *ecs,
                             move_t  move)
{
  position *pos;

  if (e->selected_entity < 0)
    return;

  pos = ecs_entity_get_component_ptr (ecs, e->selected_entity, "position");

  switch (move)
    {
    case MOVE_UP:
      pos->y -= TILE_WIDTH;
      break;

    case MOVE_DOWN:
      pos->y += TILE_WIDTH;
      break;

    case MOVE_LEFT:
      pos->x -= TILE_WIDTH;
      break;

    case MOVE_RIGHT:
      pos->x += TILE_WIDTH;
      break;
    }

  pos->x = to_grid_coordinate (pos->x);
  pos->y = to_grid_coordinate (pos->y);

  if (ecs_entity_has_component (ecs, e->selected_entity, "Tilemap"))
    tilemap_generate_tiles (ecs);

  editor_selected_entity_update_spawn_position (e, ecs, pos);
}

void
editor_move_selected_entity_to_position (editor *e,
                                         ecs_t  *ecs,
                                         double  x,
                                         double  y)
{
  position *pos;

  if (e->selected_entity < 0)
    return;

  pos = ecs_entity_get_component_ptr (ecs, e->selected_entity, "position");

  pos->x = x;
  pos->y = y;

  editor_selected_entity_update_spawn_position (e, ecs, pos);
}

void
editor_resize_selected_entity (editor   *e,
                               ecs_t    *ecs,
                               resize_t  resize)
{
  dimensions *dim;

  if (e->selected_entity < 0)
    return;

  dim = ecs_entity_get_component_ptr (ecs, e->selected_entity, "dimensions");

  switch (resize)
    {
    case WIDTH_INCREASE:
      dim->width += TILE_WIDTH;
      break;

    case WIDTH_DECREASE:
      if (dim->width > TILE_WIDTH)
        dim->width -= TILE_WIDTH;
      break;

    case HEIGHT_INCREASE:
      dim->height += TILE_WIDTH;
      break;

    case HEIGHT_DECREASE:
      if (dim->height > TILE_WIDTH)
        dim->height -= TILE_WIDTH;
      break;
    }

  if (ecs_entity_has_component (ecs, e->selected_entity, "Tilemap"))
    tilemap_generate_tiles (ecs);

  /* No entity type with spawn_state currently has variable dimensions, so
     spawn_state dimensions do not need to be updated. */
}
