/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef EDIT_ENTITY_H
#define EDIT_ENTITY_H

#include "editor.h"

typedef enum
{
  MOVE_UP,
  MOVE_DOWN,
  MOVE_LEFT,
  MOVE_RIGHT
} move_t;

typedef enum
{
  WIDTH_INCREASE,
  WIDTH_DECREASE,
  HEIGHT_INCREASE,
  HEIGHT_DECREASE
} resize_t;

/* x and y are game coordinates translated from screen coordinates with
 * to_game_coordinate. */
void editor_create_entity_at_position        (editor *e,
                                              ecs_t  *ecs,
                                              double  x,
                                              double  y);

void editor_delete_selected_entity           (editor *e,
                                              ecs_t  *ecs);

/* x and y are game coordinates translated from screen coordinates with
 * to_game_coordinate. */
void editor_attempt_select_entity            (editor *e,
                                              ecs_t  *ecs,
                                              double  x,
                                              double  y);

void editor_move_selected_entity             (editor *e,
                                              ecs_t  *ecs,
                                              move_t  move);

/* x and y are game coordinates translated from screen coordinates with
 * to_game_coordinate. */
void editor_move_selected_entity_to_position (editor *e,
                                              ecs_t  *ecs,
                                              double  x,
                                              double  y);

void editor_resize_selected_entity           (editor   *e,
                                              ecs_t    *ecs,
                                              resize_t  resize);

#endif
