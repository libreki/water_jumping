/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef EDITOR_H
#define EDITOR_H

#include "level.h"
#include "text-entry.h"

typedef struct
{
  char        lvl_name[256];

  text_entry  te;

  /* True if editor has been selected from the main menu. Set to false when
   * exiting editor. */
  bool        active;

  /* If true, editor functionality is active, if false, game systems update. */
  bool        editing;

  int         selected_entity;

  const char *creating_entity_type;

  bool        dragging;
  /* The center position of the camera dragging started. */
  SDL_FPoint  camera_start;
  /* The relative position of the mouse cursor when dragging started. */
  SDL_FPoint  drag_start;
} editor;

/* Startup function to initialize editor values without starting editor. */
void editor_init                (editor *e);

void editor_start               (editor *e);
void editor_stop                (editor *e);
void editor_toggle_editing      (editor *e,
                                 level  *lvl);

void editor_text_entry_activate (editor     *e,
                                 const char *purpose);
bool editor_text_entry_active   (editor *e);
void editor_text_entry_submit   (editor *e,
                                 level  *lvl);

#endif
