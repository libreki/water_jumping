/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include <math.h>

#include "constants.h"
#include "ecs/ecs.h"
#include "util/velocity.h"
#include "video/graphics.h"
#include "video/sprite.h"

#define ENLARGE 30

static void
puffer_update (ecs_t *ecs,
               int    entity)
{
  puffer *puff;
  velocity *v;

  puff = ecs_entity_get_component_ptr (ecs, entity, "puffer");
  v = ecs_entity_get_component_ptr (ecs, entity, "velocity");

  if (puff->accelerate)
    v->vertical += (puff->movement_direction * PUFFER_ACCELERATION);
  else
    velocity_decelerate (v, 0, PUFFER_ACCELERATION);

  if (puff->periods_elapsed >= PUFFER_MOVEMENT_PERIODS)
    {
      if (v->vertical == 0)
        {
          puff->periods_elapsed = 0;
          puff->movement_direction *= -1;
          puff->accelerate = true;
        }
    }
  else if (fabs (v->vertical) >= PUFFER_MAX_SPEED && puff->accelerate)
    {
      v->vertical = puff->movement_direction * PUFFER_MAX_SPEED;
      puff->accelerate = false;
    }
  else if (fabs (v->vertical) <= PUFFER_MIN_SPEED && !puff->accelerate)
    {
      puff->periods_elapsed++;
      if (puff->periods_elapsed < PUFFER_MOVEMENT_PERIODS)
        {
          v->vertical = puff->movement_direction * PUFFER_MIN_SPEED;
          puff->accelerate = true;
        }
    }
}

static void
puffer_update_sprite (ecs_t *ecs,
                      int    entity)
{
  puffer *puff;
  position *pos;
  dimensions *dim;
  Sprite *sprite;

  puff = ecs_entity_get_component_ptr (ecs, entity, "puffer");
  pos = ecs_entity_get_component_ptr (ecs, entity, "position");
  dim = ecs_entity_get_component_ptr (ecs, entity, "dimensions");
  sprite = ecs_entity_get_component_ptr (ecs, entity, "Sprite");

  sprite->dest.w = dim->width + ENLARGE * 2;
  sprite->dest.h = dim->height + ENLARGE * 2;
  sprite->dest.x = pos->x - ENLARGE;
  sprite->dest.y = pos->y - ENLARGE;
  if (puff->movement_direction > 0)
    {
      if (puff->accelerate)
        {
          sprite_display_texture (sprite,
                                  graphics_get_texture ("puffer_down_fast"));
        }
      else
        {
          sprite_display_texture (sprite,
                                  graphics_get_texture ("puffer_down_slow"));
        }
    }
  else
    {
      if (puff->accelerate)
        {
          sprite_display_texture (sprite,
                                  graphics_get_texture ("puffer_up_fast"));
        }
      else
        {
          sprite_display_texture (sprite,
                                  graphics_get_texture ("puffer_up_slow"));
        }
    }
}

void
puffer_system_update (ecs_t *ecs,
                      bool   condition)
{
  int *entities;

  if (!condition)
    return;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("puffer"));
  for (int i = 0; entities[i] >= 0; i++)
    {
      puffer_update (ecs, entities[i]);
      puffer_update_sprite (ecs, entities[i]);
    }

  free (entities);
}

void
puffer_system_update_sprite_only (ecs_t *ecs,
                                  bool   condition)
{
  int *entities;

  if (!condition)
    return;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("puffer"));
  for (int i = 0; entities[i] >= 0; i++)
    puffer_update_sprite (ecs, entities[i]);

  free (entities);
}
