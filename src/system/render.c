/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include "constants.h"
#include "video/sprite.h"
#include "video/tilemap.h"

#define RENDER_LAYERS 3

static void
render_queue_push (GQueue *render_queue,
                   int     entity)
{
  int *entity_ptr;

  entity_ptr = malloc (sizeof (int));
  *entity_ptr = entity;

  g_queue_push_head (render_queue, entity_ptr);
}

static int
render_queue_pop (GQueue *render_queue)
{
  int *entity_ptr;
  int entity;

  entity_ptr = g_queue_pop_tail (render_queue);
  entity = *entity_ptr;
  free (entity_ptr);

  return entity;
}

static SDL_Rect
apply_camera_offset (const Camera   *camera,
                     const SDL_Rect *dest)
{
  SDL_Rect window_dest;

  window_dest.x = (dest->x - camera->x) / camera->zoom + WINDOW_WIDTH / 2;
  window_dest.y = (dest->y - camera->y) / camera->zoom + WINDOW_HEIGHT / 2;
  window_dest.w = ceil ((float) dest->w / camera->zoom);
  window_dest.h = ceil ((float) dest->h / camera->zoom);

  return window_dest;
}

static void
render_rectangle (SDL_Renderer *renderer,
                  const Camera *camera,
                  position     *pos,
                  dimensions   *dim,
                  SDL_Color     color,
                  bool          fill)
{
  SDL_Rect rect;

  rect.x = pos->x;
  rect.y = pos->y;
  rect.w = dim->width;
  rect.h = dim->height;

  rect = apply_camera_offset (camera, &rect);

  SDL_SetRenderDrawColor (renderer, color.r, color.g, color.b, color.a);

  if (fill)
    SDL_RenderFillRect (renderer, &rect);
  else
    {
      /* Hack to draw outline thicker than 1 pixel. */
      for (int n = 0; n < 5; n++)
        {
          SDL_Rect outline;

          outline.x = rect.x + n;
          outline.y = rect.y + n;
          outline.w = rect.w - 2 * n;
          outline.h = rect.h - 2 * n;

          SDL_RenderDrawRect (renderer, &outline);
        }
    }
}

static void
render_sprite (SDL_Renderer *renderer,
               const Camera *camera,
               Sprite       *sprite)
{
  SDL_Rect dest;

  switch (sprite->type)
    {
    case STATIC_SPRITE:
      /* TODO: Check that on-screen */
      {
        dest = apply_camera_offset (camera, &sprite->dest);

        SDL_RenderCopy (renderer,
                        (SDL_Texture *) sprite->texture->atlas,
                        &sprite->texture->src, &dest);
      }
      break;

    case ANIMATED_SPRITE:
      /* TODO: Check that on-screen */
      {
        const Frame *frame;

        dest = apply_camera_offset (camera, &sprite->dest);
        frame = animation_get_current_frame (sprite->animation,
                                             &sprite->animation_timer);
        SDL_RenderCopy (renderer,
                        (SDL_Texture *) sprite->animation->atlas,
                        &frame->src, &dest);
      }
      break;
    }
}

static void
render_tilemap (SDL_Renderer *renderer,
                const Camera *camera,
                Tilemap      *tilemap)
{
  for (int i = 0; i < tilemap->tiles_length; i++)
    render_sprite (renderer, camera, &tilemap->tiles[i]);
}

void
render_system_update (ecs_t        *ecs,
                      SDL_Renderer *renderer,
                      const Camera *camera,
                      editor       *e,
                      bool          condition)
{
  int *entities;
  GQueue *render_queues[RENDER_LAYERS];

  if (!condition)
    return;

  for (int i = 0; i < RENDER_LAYERS; i++)
    render_queues[i] = g_queue_new ();

  /* Populate render queues. */
  entities = ecs_entities_find (ecs, renderable_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      renderable *rend;

      rend = ecs_entity_get_component_ptr (ecs, entities[i], "renderable");
      render_queue_push (render_queues[rend->layer], entities[i]);
    }
  free (entities);

  /* Some entities are only rendered while in the editor. */
  if (e->editing)
    {
      entities = ecs_entities_find (ecs, editor_renderable_sig);
      for (int i = 0; entities[i] >= 0; i++)
        {
          editor_renderable *erend;

          erend = ecs_entity_get_component_ptr (ecs, entities[i],
                                                "editor_renderable");
          render_queue_push (render_queues[erend->layer], entities[i]);
        }
      free (entities);
    }

  /* Iterate over all render queues and render entities. */
  for (int i = 0; i < RENDER_LAYERS; i++)
    {
      while (!g_queue_is_empty (render_queues[i]))
        {
          int entity;

          entity = render_queue_pop (render_queues[i]);

          if (ecs_entity_has_component (ecs, entity, "Sprite"))
            {
              Sprite *sprite;

              sprite = ecs_entity_get_component_ptr (ecs, entity, "Sprite");
              render_sprite (renderer, camera, sprite);
            }
          else if (ecs_entity_has_component (ecs, entity, "Tilemap"))
            {
              Tilemap *tilemap;

              tilemap = ecs_entity_get_component_ptr (ecs, entity, "Tilemap");
              render_tilemap (renderer, camera, tilemap);
            }
          else
            {
              position *pos;
              dimensions *dim;

              pos = ecs_entity_get_component_ptr (ecs, entity, "position");
              dim = ecs_entity_get_component_ptr (ecs, entity, "dimensions");
              render_rectangle (renderer, camera, pos, dim,
                                (SDL_Color) { 255, 125, 0, 255 }, true);
            }
        }
    }

  /* If in the editor and an entity is selected, draw a highlight around it. */
  if (e->editing && e->selected_entity >= 0)
    {
      position *pos;
      dimensions *dim;

      pos = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                          "position");
      dim = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                          "dimensions");
      render_rectangle (renderer, camera, pos, dim,
                        (SDL_Color) { 255, 255, 0, 255 }, false);
    }

  for (int i = 0; i < RENDER_LAYERS; i++)
    g_queue_free (render_queues[i]);
}
