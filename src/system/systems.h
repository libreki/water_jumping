/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SYSTEMS_H
#define SYSTEMS_H

#include "ecs/ecs.h"
#include "editor/editor.h"
#include "level.h"
#include "util/input.h"
#include "video/camera.h"

void camera_system_update             (ecs_t  *ecs,
                                       Camera *camera,
                                       bool    condition);

void checkpoint_system_update         (ecs_t *ecs,
                                       level *lvl,
                                       bool   condition);

void physics_system_update            (ecs_t *ecs,
                                       bool   condition);

void player_system_update             (ecs_t *ecs,
                                       Input  input,
                                       level *lvl,
                                       bool   condition);

void puffer_system_update             (ecs_t *ecs,
                                       bool   condition);
void puffer_system_update_sprite_only (ecs_t *ecs,
                                       bool   condition);

void render_system_update             (ecs_t        *ecs,
                                       SDL_Renderer *renderer,
                                       const Camera *camera,
                                       editor       *e,
                                       bool          condition);

void scene_warp_system_update         (ecs_t *ecs,
                                       level *lvl,
                                       bool   condition);

#endif
