/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include <math.h>

#include "constants.h"
#include "util/entity.h"

void
camera_system_update (ecs_t  *ecs,
                      Camera *camera,
                      bool    condition)
{
  int *entities;
  int plr;
  position *plr_pos;
  dimensions *plr_dim;

  if (!condition)
    return;

  plr = player_find (ecs);
  plr_pos = ecs_entity_get_component_ptr (ecs, plr, "position");
  plr_dim = ecs_entity_get_component_ptr (ecs, plr, "dimensions");

  /* By default the camera is centered on player. */
  camera->x = plr_pos->x + plr_dim->width / 2;
  camera->y = plr_pos->y + plr_dim->height / 2;
  camera->zoom = GAME_ZOOM;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("camera"));
  for (int i = 0; entities[i] >= 0; i++)
    {
      if (rects_overlap (ecs, plr, entities[i]))
        {
          struct camera *cam;
          position *cam_pos;
          dimensions *cam_dim;
          double x_dist, y_dist, max_dist;

          cam = ecs_entity_get_component_ptr (ecs, entities[i], "camera");
          cam_pos = ecs_entity_get_component_ptr (ecs, entities[i],
                                                  "position");
          cam_dim = ecs_entity_get_component_ptr (ecs, entities[i],
                                                  "dimensions");

          x_dist = (fabs (cam_pos->x + cam_dim->width / 2 - camera->x)
                    / (cam_dim->width / 2));
          y_dist = (fabs (cam_pos->y + cam_dim->height / 2 - camera->y)
                    / (cam_dim->height / 2));

          max_dist = fmax (x_dist, y_dist);
          if (max_dist <= cam->threshold)
            {
              /* Player is within the threshold for camera lock to be in full
               * effect. */
              camera->x = cam->center_x;
              camera->y = cam->center_y;
              camera->zoom = cam->zoom;
            }
          else if (max_dist < 1)
            {
              double w;

              /* Player is within the effect range of the camera, but not
               * within the threshold for the camera lock to be in full effect.
               * The position of the view is a weighted average of the
               * player's center and the camera center values based on how
               * close the player is to the threshold. The size of the view is
               * a similar average of the standard game_view size and the
               * camera zoom value. */

              /* Weight of standard view values. */
              w = fabs (cam->threshold - max_dist) / (1 - cam->threshold);

              camera->x = floor (w * camera->x + (1 - w) * cam->center_x);
              camera->y = floor (w * camera->y + (1 - w) * cam->center_y);
              camera->zoom = w * GAME_ZOOM + (1 - w) * cam->zoom;
            }

          break;
        }
    }

  free (entities);
}
