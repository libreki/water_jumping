/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include "util/entity.h"

void
scene_warp_system_update (ecs_t *ecs,
                          level *lvl,
                          bool   condition)
{
  int *entities;
  int plr;

  if (!condition)
    return;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("scene_warp"));
  plr = player_find (ecs);
  for (int i = 0; entities[i] >= 0; i++)
    {
      if (rects_overlap (ecs, entities[i], plr))
        {
          scene_warp *sw;
          ecs_t *warp_ecs;
          int warp_plr;
          position *warp_plr_pos;
          velocity *warp_plr_v;

          sw = ecs_entity_get_component_ptr (ecs, entities[i], "scene_warp");

          lvl->current_scene = sw->scene_index;
          warp_ecs = level_current_ecs (lvl);

          warp_plr = player_find (warp_ecs);
          warp_plr_pos = ecs_entity_get_component_ptr (warp_ecs, warp_plr,
                                                       "position");
          warp_plr_v = ecs_entity_get_component_ptr (warp_ecs, warp_plr,
                                                     "velocity");
          warp_plr_pos->x = sw->player_x;
          warp_plr_pos->y = sw->player_y;
          warp_plr_v->horizontal = 0;
          warp_plr_v->vertical = 0;

          break;
        }
    }

  free (entities);
}
