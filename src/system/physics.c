/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include <math.h>

#include "util/entity.h"
#include "util/velocity.h"

static void
movement_system_update (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, movable_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      position *pos;
      velocity *v;

      pos = ecs_entity_get_component_ptr (ecs, entities[i], "position");
      v = ecs_entity_get_component_ptr (ecs, entities[i], "velocity");

      pos->x += v->horizontal;
      pos->y += v->vertical;
    }

  free (entities);
}

static void
gravity_system_update (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, gravity_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      gravity *g;
      velocity *v;

      g = ecs_entity_get_component_ptr (ecs, entities[i], "gravity");
      v = ecs_entity_get_component_ptr (ecs, entities[i], "velocity");

      velocity_accelerate (v, 0, g->acceleration);
    }

  free (entities);
}

static void
collision_system_update (ecs_t *ecs)
{
  int *entities, *obstacles;

  entities = ecs_entities_find (ecs, collidable_sig);
  obstacles = ecs_entities_find (ecs, obstacle_sig);

  for (int i = 0; entities[i] >= 0; i++)
    {
      position *pos, *obs_pos;
      dimensions *dim, *obs_dim;
      collision *col;
      velocity *v;
      double x;

      pos = ecs_entity_get_component_ptr (ecs, entities[i], "position");
      dim = ecs_entity_get_component_ptr (ecs, entities[i], "dimensions");
      col = ecs_entity_get_component_ptr (ecs, entities[i], "collision");
      v = ecs_entity_get_component_ptr (ecs, entities[i], "velocity");

      col->up = false;
      col->down = false;
      col->left = false;
      col->right = false;

      x = pos->x;

      pos->x = pos->x - v->horizontal;
      for (int j = 0; obstacles[j] >= 0; j++)
        {
          if (rects_overlap (ecs, entities[i], obstacles[j]))
            {
              obs_pos = ecs_entity_get_component_ptr (ecs, obstacles[j],
                                                      "position");
              obs_dim = ecs_entity_get_component_ptr (ecs, obstacles[j],
                                                      "dimensions");

              if (v->vertical > 0)
                {
                  /* Entity hit ground */
                  pos->y = obs_pos->y - dim->height;
                  col->down = true;
                }
              else
                {
                  /* Entity hit ceiling */
                  pos->y = obs_pos->y + obs_dim->height;
                  col->up = true;
                }

              v->vertical = 0;
            }
        }

      pos->x = x;
      for (int j = 0; obstacles[j] >= 0; j++)
        {
          if (rects_overlap (ecs, entities[i], obstacles[j]))
            {
              obs_pos = ecs_entity_get_component_ptr (ecs, obstacles[j],
                                                      "position");
              obs_dim = ecs_entity_get_component_ptr (ecs, obstacles[j],
                                                      "dimensions");

              if (v->horizontal > 0)
                {
                  /* Entity hit right wall */
                  pos->x = obs_pos->x - dim->width;
                  col->right = true;
                }
              else
                {
                  /* Entity hit left wall */
                  pos->x = obs_pos->x + obs_dim->width;
                  col->left = true;
                }

              v->horizontal = 0;
              continue;
            }
        }
    }

  free (entities);
  free (obstacles);
}

void
physics_system_update (ecs_t *ecs,
                       bool   condition)
{
  if (!condition)
    return;

  gravity_system_update (ecs);
  movement_system_update (ecs);
  collision_system_update (ecs);
}
