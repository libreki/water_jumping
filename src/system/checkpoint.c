/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include "util/entity.h"
#include "util/spawn-state.h"

void
checkpoint_system_update (ecs_t *ecs,
                          level *lvl,
                          bool   condition)
{
  int *entities;
  int plr;

  if (!condition)
    return;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("checkpoint"));
  plr = player_find (ecs);
  for (int i = 0; entities[i] >= 0; i++)
    {
      checkpoint *cp;

      cp = ecs_entity_get_component_ptr (ecs, entities[i], "checkpoint");

      if (cp->activated)
        continue;

      if (rects_overlap (ecs, entities[i], plr))
        {
          position *cp_pos;
          spawn_state *plr_ss;
          position *plr_ss_pos;

          cp->activated = true;

          plr_ss = ecs_entity_get_component_ptr (ecs, plr, "spawn_state");
          plr_ss_pos = spawn_state_get_component_ptr (plr_ss, "position");
          cp_pos = ecs_entity_get_component_ptr (ecs, entities[i], "position");

          plr_ss_pos->x = cp_pos->x;
          plr_ss_pos->y = cp_pos->y;

          level_save_current_state (lvl);
        }
    }

  free (entities);
}
