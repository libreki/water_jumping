/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "systems.h"

#include <math.h>

#include "constants.h"
#include "ecs/ecs.h"
#include "util/entity.h"
#include "util/velocity.h"

static bool
player_touches_entity (ecs_t    *ecs,
                       int       plr,
                       uint64_t  signature)
{
  int *entities;

  entities = ecs_entities_find (ecs, signature);
  for (int i = 0; entities[i] >= 0; i++)
    {
      if (rects_overlap (ecs, plr, entities[i]))
        {
          free (entities);
          return true;
        }
    }

  free (entities);
  return false;
}

static void
player_swim (velocity *v,
             gravity  *g,
             Input     input)
{
  double abs, scale;

  g->acceleration = 0;
  v->max_horizontal = MAX_WATER_SPEED;
  v->max_vertical = MAX_WATER_SPEED;

  if (input.current.left && !input.current.right)
    v->horizontal -= WATER_ACCELERATION;
  else if (input.current.right && !input.current.left)
    v->horizontal += WATER_ACCELERATION;
  else
    velocity_decelerate (v, WATER_DECELERATION, 0);

  if (input.current.up && !input.current.down)
    v->vertical -= WATER_ACCELERATION;
  else if (input.current.down && !input.current.up)
    v->vertical += WATER_ACCELERATION;
  else
    velocity_decelerate (v, 0, WATER_DECELERATION);

  abs = sqrt (pow (v->horizontal, 2) + pow (v->vertical, 2));
  if (abs > MAX_WATER_SPEED)
    {
      scale = MAX_WATER_SPEED / abs;
      v->horizontal *= scale;
      v->vertical *= scale;
    }
}

static void
player_jump (player    *plr,
             velocity  *v,
             gravity   *g,
             collision *col,
             Input      input)
{
  v->max_vertical = TERMINAL_VELOCITY;

  if (v->horizontal == 0 && !col->down)
    {
      if (col->left)
        plr->can_wall_jump_from = -1;
      else if (col->right)
        plr->can_wall_jump_from = 1;
    }
  else if (v->horizontal != 0)
    plr->can_wall_jump_from = 0;

  if (input.current.jump && !input.previous.jump)
    {
      if (col->down)
        {
          v->vertical = JUMP_SPEED;
          g->acceleration = JUMP_GRAVITY;
        }
      else if (plr->can_wall_jump_from != 0)
        {
          /* TODO: Remove magic number */
          v->horizontal = plr->can_wall_jump_from < 0 ? 24 : -24;
          v->vertical = JUMP_SPEED;
          g->acceleration = JUMP_GRAVITY;
          /* TODO: Magic number */
          timer_start (&plr->wall_jump_slow_timer, 45);
        }
    }
  else if (!input.current.jump || v->vertical > 0)
    g->acceleration = GRAVITY;
}

static void
player_walk (player    *plr,
             velocity  *v,
             collision *col,
             Input      input)
{
  bool decelerated;
  double acceleration;

  /* We don't want to call decelerate twice in a single update. */
  decelerated = false;
  if (fabs (v->horizontal) > MAX_AIR_SPEED && col->down)
    {
      decelerated = true;
      velocity_decelerate (v, AIR_ACCELERATION, 0);
    }

  if (fabs (v->horizontal) < MAX_AIR_SPEED)
    v->max_horizontal = MAX_AIR_SPEED;
  else
    v->max_horizontal = fabs (v->horizontal);

  acceleration = (AIR_ACCELERATION
                  * timer_progress (&plr->wall_jump_slow_timer));

  if (input.current.left && !input.current.right)
    velocity_accelerate (v, -acceleration, 0);
  else if (input.current.right && !input.current.left)
    velocity_accelerate (v, acceleration, 0);
  else if (!decelerated && col->down)
    velocity_decelerate (v, AIR_ACCELERATION, 0);
}

void
player_system_update (ecs_t *ecs,
                      Input  input,
                      level *lvl,
                      bool   condition)
{
  int entity;
  player *plr;
  velocity *v;
  gravity *g;
  collision *col;

  if (!condition)
    return;

  entity = player_find (ecs);

  plr = ecs_entity_get_component_ptr (ecs, entity, "player");
  v = ecs_entity_get_component_ptr (ecs, entity, "velocity");
  g = ecs_entity_get_component_ptr (ecs, entity, "gravity");
  col = ecs_entity_get_component_ptr (ecs, entity, "collision");

  if (player_touches_entity (ecs, entity,
                             ecs_entity_type_signature ("level_end")))
    {
      lvl->exit = true;
      return;
    }

  if (player_touches_entity (ecs, entity, danger_sig))
    {
      lvl->load_checkpoint = true;
      return;
    }

  if (player_touches_entity (ecs, entity,
                             ecs_entity_type_signature ("water")))
    {
      player_swim (v, g, input);
    }
  else
    {
      if (g->acceleration == 0)
        {
          g->acceleration = GRAVITY;
          v->horizontal *= WATER_TO_AIR_MULTIPLIER;
          v->vertical *= WATER_TO_AIR_MULTIPLIER;
        }

      player_jump (plr, v, g, col, input);
      player_walk (plr, v, col, input);
    }
}
