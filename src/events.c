/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "events.h"

#include "constants.h"
#include "editor/editor-camera.h"
#include "editor/edit-entity.h"
#include "menu/menu.h"
#include "settings.h"
#include "util/input.h"

static void
menu_events (SDL_Event *event,
             editor    *e,
             level     *lvl)
{
  if (event->type == SDL_KEYUP
      && event->key.keysym.sym == settings_get_bind ("control_pause"))
    {
      menu_pause_pressed ();
    }

  if (!menu_is_open ())
    return;

  switch (event->type)
    {
    case SDL_KEYDOWN:
      if (settings_waiting_for_bind ())
        break;
      if (event->key.keysym.sym == settings_get_bind ("control_up"))
        menu_up ();
      else if (event->key.keysym.sym == settings_get_bind ("control_down"))
        menu_down ();
      else if (event->key.keysym.sym == settings_get_bind ("control_left"))
        menu_left ();
      else if (event->key.keysym.sym == settings_get_bind ("control_right"))
        menu_right ();
      break;

    case SDL_KEYUP:
      if (settings_waiting_for_bind ())
        settings_enter_bind (event->key.keysym.sym);
      else if (event->key.keysym.sym == settings_get_bind ("control_jump"))
        menu_select_item (lvl, e);
      break;

    default:
      break;
    }
}

static void
editor_events (SDL_Event *event,
               editor    *e,
               level     *lvl,
               Camera    *camera)
{
  ecs_t *ecs;

  if (e->active && event->type == SDL_KEYUP)
    {
      switch (event->key.keysym.sym)
        {
        case SDLK_F4:
          if (!menu_is_open ())
            editor_toggle_editing (e, lvl);
          break;

        case SDLK_F12:
          /* TODO: Feedback */
          level_xml_write (lvl, e->lvl_name);
          printf ("Saved level to ./data/level/%s.xml.\n",
                  e->lvl_name);
          break;

        default:
          break;
        }
    }

  if (!e->editing)
    return;

  if (editor_text_entry_active (e))
    {
      switch (event->type)
        {
        case SDL_KEYDOWN:
          switch (event->key.keysym.sym)
            {
            case SDLK_BACKSPACE:
              text_entry_delete_char (&e->te);
              break;

            case SDLK_RETURN:
              editor_text_entry_submit (e, lvl);
              break;

            default:
              break;
            }
          break;

        case SDL_TEXTINPUT:
          text_entry_enter_char (&e->te, event->text.text[0]);
          break;

        default:
          break;
        }
    }

  if (menu_is_open ())
    return;

  ecs = level_current_ecs (lvl);
  switch (event->type)
    {
    case SDL_KEYDOWN:
      switch (event->key.keysym.sym)
        {
        case SDLK_UP:
          if (event->key.keysym.mod & KMOD_LSHIFT)
            editor_resize_selected_entity (e, ecs, HEIGHT_DECREASE);
          else
            editor_move_selected_entity (e, ecs, MOVE_UP);
          break;

        case SDLK_DOWN:
          if (event->key.keysym.mod & KMOD_LSHIFT)
            editor_resize_selected_entity (e, ecs, HEIGHT_INCREASE);
          else
            editor_move_selected_entity (e, ecs, MOVE_DOWN);
          break;

        case SDLK_LEFT:
          if (event->key.keysym.mod & KMOD_LSHIFT)
            editor_resize_selected_entity (e, ecs, WIDTH_DECREASE);
          else
            editor_move_selected_entity (e, ecs, MOVE_LEFT);
          break;

        case SDLK_RIGHT:
          if (event->key.keysym.mod & KMOD_LSHIFT)
            editor_resize_selected_entity (e, ecs, WIDTH_INCREASE);
          else
            editor_move_selected_entity (e, ecs, MOVE_RIGHT);
          break;

        default:
          break;
        }
      break;

    case SDL_KEYUP:
      switch (event->key.keysym.sym)
        {
        case SDLK_c:
          if (!editor_text_entry_active (e))
            menu_open (COMPONENT_MENU_T);
          break;

        case SDLK_e:
          if (!editor_text_entry_active (e))
            menu_open (ENTITY_MENU_T);
          break;

        case SDLK_r:
          {
            int xi, yi;
            double x, y;

            SDL_GetMouseState (&xi, &yi);

            x = to_game_coordinate (camera, xi, X_AXIS, true);
            y = to_game_coordinate (camera, yi, Y_AXIS, true);

            editor_create_entity_at_position (e, ecs, x, y);
          }
          break;

        case SDLK_DELETE:
          editor_delete_selected_entity (e, ecs);
          break;

        default:
          break;
        }
      break;

    case SDL_MOUSEWHEEL:
      if (event->wheel.y < 0)
        camera->zoom += EDITOR_VIEW_ZOOM_AMOUNT;
      else
        camera->zoom -= EDITOR_VIEW_ZOOM_AMOUNT;
      break;

    case SDL_MOUSEBUTTONDOWN:
      if (event->button.button == SDL_BUTTON_LEFT)
        {
          double x, y;

          x = to_game_coordinate (camera, event->button.x, X_AXIS, false);
          y = to_game_coordinate (camera, event->button.y, Y_AXIS, false);

          editor_camera_drag_start (e, camera, x, y);
        }
      break;

    case SDL_MOUSEMOTION:
      if (SDL_GetMouseState (NULL, NULL) & SDL_BUTTON (1))
        {
          /* Left mouse button held. */
          double x, y;

          x = to_game_coordinate (camera, event->motion.x, X_AXIS, false);
          y = to_game_coordinate (camera, event->motion.y, Y_AXIS, false);

          editor_camera_drag_update (e, camera, x, y);
        }
      else if (e->editing && SDL_GetMouseState (NULL, NULL) & SDL_BUTTON (3))
        {
          /* Right mouse button held. */
          double x, y;

          x = to_game_coordinate (camera, event->motion.x, X_AXIS, true);
          y = to_game_coordinate (camera, event->motion.y, Y_AXIS, true);

          editor_move_selected_entity_to_position (e, ecs, x, y);
        }
      break;

    case SDL_MOUSEBUTTONUP:
      if (event->button.button == SDL_BUTTON_LEFT)
        {
          double x, y;

          x = to_game_coordinate (camera, event->button.x, X_AXIS, true);
          y = to_game_coordinate (camera, event->button.y, Y_AXIS, true);

          editor_camera_drag_finish (e);
          editor_attempt_select_entity (e, ecs, x, y);
        }
      break;

    default:
      break;
    }
}

void
poll_events (bool   *running,
             editor *e,
             level  *lvl,
             Camera *camera)
{
  SDL_Event event;

  while (SDL_PollEvent (&event))
    {
      if (event.type == SDL_QUIT)
        *running = false;

      menu_events (&event, e, lvl);
      editor_events (&event, e, lvl, camera);
    }
}
