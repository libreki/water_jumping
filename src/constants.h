/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

void        constants_init    (void);
void        constants_destroy (void);

const char *constant_get      (const char *name);

/* TODO: Don't include all constants in this file? */

#define GRAVITY atof (constant_get ("gravity"))
#define JUMP_GRAVITY atof (constant_get ("jump_gravity"))
#define TERMINAL_VELOCITY atof (constant_get ("terminal_velocity"))
#define JUMP_SPEED atof (constant_get ("jump_speed"))
#define AIR_ACCELERATION atof (constant_get ("air_acceleration"))
#define MAX_AIR_SPEED atof (constant_get ("max_air_speed"))
#define WATER_ACCELERATION atof (constant_get ("water_acceleration"))
#define WATER_DECELERATION atof (constant_get ("water_deceleration"))
#define MAX_WATER_SPEED atof (constant_get ("max_water_speed"))
#define WATER_TO_AIR_MULTIPLIER atof (constant_get ("water_to_air_multiplier"))
#define BUNNY_HOP_BOOST atof (constant_get ("bunny_hop_boost"))
#define WALL_JUMP_ACCELERATION atof (constant_get ("wall_jump_acceleration"))

#define PUFFER_ACCELERATION atof (constant_get ("puffer_acceleration"))
#define PUFFER_MAX_SPEED atof (constant_get ("puffer_max_speed"))
#define PUFFER_MIN_SPEED atof (constant_get ("puffer_min_speed"))
#define PUFFER_MOVEMENT_PERIODS atoi (constant_get ("puffer_movement_periods"))
/* TODO: This has no value */
#define PUFFER_MOVEMENT_LENGTH atof (constant_get ("puffer_movement_length"))

#define GAME_ZOOM atof (constant_get ("game_zoom"))
#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
#define FRAMERATE 60
#define TILE_WIDTH 128
#define EDITOR_VIEW_ZOOM_AMOUNT 0.1
#define WATER_FRAME_TIME 5

#endif
