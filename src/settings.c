/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "settings.h"

#include <glib.h>

#include "util/xml.h"

static SDL_Keycode settings_get_bind_from (GHashTable *src,
                                           const char *control);
static void        settings_update_bind   (SDL_Keycode key);

static GHashTable *settings = NULL;
static GHashTable *default_settings = NULL;

static const char *changing_bind = NULL;

static SDL_Keycode *
malloc_key (SDL_Keycode key)
{
  SDL_Keycode *key_ptr;

  key_ptr = malloc (sizeof (SDL_Keycode));
  *key_ptr = key;

  return key_ptr;
}

/* REUSE-IgnoreStart */
static xmlChar *license = (xmlChar *)
"\n  Copyright (C) 2022-2023 libreki\n"
"\n"
"  This program is free software: you can redistribute it and/or modify\n"
"  it under the terms of the GNU General Public License as published by\n"
"  the Free Software Foundation, either version 3 of the License, or\n"
"  (at your option) any later version.\n"
"\n"
"  This program is distributed in the hope that it will be useful,\n"
"  but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"  GNU General Public License for more details.\n"
"\n"
"  You should have received a copy of the GNU General Public License\n"
"  along with this program.  If not, see <https://www.gnu.org/licenses/>.\n"
"\n"
"  SPDX-License-Identifier: GPL-3.0-or-later\n";
/* REUSE-IgnoreEnd */

static void
settings_xml_write_setting (xmlTextWriter *writer,
                            const char    *key,
                            int            value)
{
  xml_start_element (writer, "setting");

  xml_write_attribute (writer, "key", key);
  xml_write_attribute_int (writer, "value", value);

  xml_end_element (writer);
}

static void
settings_xml_create_from (GHashTable *src)
{
  xmlTextWriter *writer;

  writer = xml_new_writer ("./data/config/settings.xml");

  xmlTextWriterWriteComment (writer, license);

  xml_start_element (writer, "settings");

  settings_xml_write_setting (writer, "control_up",
                              settings_get_bind_from (src, "control_up"));
  settings_xml_write_setting (writer, "control_down",
                              settings_get_bind_from (src, "control_down"));
  settings_xml_write_setting (writer, "control_left",
                              settings_get_bind_from (src, "control_left"));
  settings_xml_write_setting (writer, "control_right",
                              settings_get_bind_from (src, "control_right"));
  settings_xml_write_setting (writer, "control_jump",
                              settings_get_bind_from (src, "control_jump"));
  settings_xml_write_setting (writer, "control_pause",
                              settings_get_bind_from (src, "control_pause"));

  xml_end_element (writer);

  xmlTextWriterEndDocument (writer);
  xmlFreeTextWriter (writer);
}

static void
settings_load_from_xml (void)
{
  xmlDoc *doc;
  xmlNode *node;

  if (settings)
    g_hash_table_destroy (settings);

  settings = g_hash_table_new_full (g_str_hash, g_str_equal, free, free);

  doc = xmlReadFile ("./data/config/settings.xml", NULL, 0);
  if (!doc)
    {
      settings_xml_create_from (default_settings);
      doc = xml_read_file_or_exit ("./data/config/settings.xml");
    }

  node = xmlDocGetRootElement (doc);
  xml_assert_element (node, "settings");

  node = node->children;
  while (true)
    {
      xml_assert_element (node, "setting");

      g_hash_table_insert (settings,
                           xml_get_prop_string (node, "key"),
                           malloc_key (xml_get_prop_int (node, "value")));

      XML_NEXT_OR_BREAK (node);
    }

  xmlFreeDoc (doc);
}

void
settings_global_init (void)
{
  default_settings = g_hash_table_new_full (g_str_hash, g_str_equal,
                                            NULL, free);

  g_hash_table_insert (default_settings, "control_up",
                       malloc_key (SDLK_w));
  g_hash_table_insert (default_settings, "control_down",
                       malloc_key (SDLK_s));
  g_hash_table_insert (default_settings, "control_left",
                       malloc_key (SDLK_a));
  g_hash_table_insert (default_settings, "control_right",
                       malloc_key (SDLK_d));
  g_hash_table_insert (default_settings, "control_jump",
                       malloc_key (SDLK_SPACE));
  g_hash_table_insert (default_settings, "control_pause",
                       malloc_key (SDLK_ESCAPE));

  settings_load_from_xml ();
}

void
settings_global_destroy (void)
{
  g_hash_table_destroy (settings);
  g_hash_table_destroy (default_settings);
}

void
settings_reset (void)
{
  settings_xml_create_from (default_settings);
  settings_load_from_xml ();
}

static SDL_Keycode
settings_get_bind_from (GHashTable *src,
                        const char *control)
{
  if (!g_hash_table_contains (src, control))
    {
      fprintf (stderr,
               "SETTINGS ERROR: Attempted to get bind of unknown control %s.",
               control);
      exit (1);
    }

  return *(SDL_Keycode *) g_hash_table_lookup (src, control);
}

SDL_Keycode
settings_get_bind (const char *control)
{
  return settings_get_bind_from (settings, control);
}

static void
settings_update_bind (SDL_Keycode key)
{
  const char **setting_names;

  setting_names = (const char **) g_hash_table_get_keys_as_array (settings,
                                                                  NULL);
  for (int i = 0; setting_names[i]; i++)
    {
      if (settings_get_bind (setting_names[i]) == key)
        {
          g_hash_table_replace
            (settings, g_strdup (setting_names[i]),
             malloc_key (settings_get_bind (changing_bind)));
          break;
        }
    }

  free (setting_names);

  g_hash_table_replace (settings, g_strdup (changing_bind), malloc_key (key));

  settings_xml_create_from (settings);
}

void
settings_begin_bind_change (const char *control)
{
  changing_bind = control;
}

void
settings_enter_bind (SDL_Keycode key)
{
  if (settings_waiting_for_bind ())
    settings_update_bind (key);

  changing_bind = NULL;
}

bool
settings_waiting_for_bind (void)
{
  return (changing_bind != NULL);
}
