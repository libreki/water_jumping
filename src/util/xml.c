/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "xml.h"

#include <stdio.h>
#include <string.h>

#include <glib.h>

xmlDoc *
xml_read_file_or_exit (const char *filename)
{
  xmlDoc *doc;

  doc = xmlReadFile (filename, NULL, 0);
  if (!doc)
    {
      fprintf (stderr, "XML ERROR: Failed to parse %s.\n", filename);
      exit (1);
    }

  return doc;
}

static void
xml_has_prop_or_exit (const xmlNode *node,
                      const char    *name,
                      const char    *prop_type)
{
  if (!xml_has_prop (node, name))
    {
      fprintf (stderr,
               "XML ERROR: Attempted to get nonexistent %s property %s from "
               "element %s.\n", prop_type, name, node->name);
      exit (1);
    }
}

bool
xml_has_prop (const xmlNode *node,
              const char    *name)
{
  return xmlHasProp (node, (const xmlChar *) name);
}

char *
xml_get_prop_string (const xmlNode *node,
                     const char    *name)
{
  xml_has_prop_or_exit (node, name, "string");
  return (char *) xmlGetProp (node, (const xmlChar *) name);
}

double
xml_get_prop_double (const xmlNode *node,
                     const char    *name)
{
  char *prop;
  double ret;

  xml_has_prop_or_exit (node, name, "double");
  prop = (char *) xmlGetProp (node, (const xmlChar *) name);
  ret = atof (prop);

  free (prop);

  return ret;
}

int
xml_get_prop_int (const xmlNode *node,
                  const char    *name)
{
  char *prop;
  int ret;

  xml_has_prop_or_exit (node, name, "int");
  prop = (char *) xmlGetProp (node, (const xmlChar *) name);
  ret = atoi (prop);

  free (prop);

  return ret;
}

bool
xml_get_prop_bool (const xmlNode *node,
                   const char    *name)
{
  char *prop;
  bool ret;

  xml_has_prop_or_exit (node, name, "bool");
  prop = (char *) xmlGetProp (node, (const xmlChar *) name);
  ret = g_str_equal (prop, "true");

  free (prop);

  return ret;
}

bool
xml_element_is (const xmlNode *node,
                const char    *name)
{
  return g_str_equal (node->name, name);
}

void
xml_assert_element (const xmlNode *node,
                    const char    *name)
{
  if (!xml_element_is (node, name))
    {
      fprintf (stderr, "XML ERROR: Expected element %s, got %s.",
               name, node->name);
      exit (1);
    }
}

xmlTextWriter *
xml_new_writer (const char *uri)
{
  xmlTextWriter *writer;

  writer = xmlNewTextWriterFilename (uri, 0);

  xmlTextWriterStartDocument (writer, "1.0", "UTF-8", NULL);
  xmlTextWriterSetIndent (writer, 1);
  xmlTextWriterSetIndentString (writer, (xmlChar *) "  ");

  return writer;
}

void
xml_start_element (xmlTextWriter *writer,
                   const char    *name)
{
  xmlTextWriterStartElement (writer, (xmlChar *) name);
}

void
xml_end_element (xmlTextWriter *writer)
{
  xmlTextWriterEndElement (writer);
}

void
xml_write_attribute (xmlTextWriter *writer,
                     const char    *name,
                     const char    *content)
{
  xmlTextWriterWriteAttribute (writer, (xmlChar *) name, (xmlChar *) content);
}

void
xml_write_attribute_int (xmlTextWriter *writer,
                         const char    *name,
                         int            content)
{
  char *content_str;

  content_str = g_strdup_printf ("%d", content);
  xml_write_attribute (writer, name, content_str);

  free (content_str);
}

void
xml_write_attribute_double (xmlTextWriter *writer,
                            const char    *name,
                            double         content)
{
  char *content_str;

  content_str = g_strdup_printf ("%f", content);
  xml_write_attribute (writer, name, content_str);

  free (content_str);
}

void
xml_write_attribute_bool (xmlTextWriter *writer,
                          const char    *name,
                          bool           content)
{
  xml_write_attribute (writer, name, content ? "true" : "false");
}
