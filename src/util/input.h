/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>

#include "video/camera.h"

typedef struct
{
  bool up;
  bool down;
  bool left;
  bool right;
  bool jump;
} Keys_Pressed;

typedef struct
{
  Keys_Pressed current;
  Keys_Pressed previous;
} Input;

void   input_update       (Input *input);

typedef enum
{
  X_AXIS = 0,
  Y_AXIS = 1
} Coordinate_Axis;

double to_game_coordinate (const Camera    *camera,
                           double           value,
                           Coordinate_Axis  axis,
                           bool             absolute);

#endif
