/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spawn-state.h"

void
spawn_state_ptr_copy (component_t *src,
                      component_t *dest)
{
  spawn_state *ss_src, *ss_dest;
  const char **component_names;

  ss_src = src->ptr;
  ss_dest = dest->ptr;

  component_names = ecs_components_registered ();
  for (int i = 0; component_names[i]; i++)
    {
      ecs_component_copy (spawn_state_get_component (ss_src,
                                                     component_names[i]),
                          spawn_state_get_component (ss_dest,
                                                     component_names[i]));
    }

  free (component_names);
}

void
spawn_state_ptr_destroy (component_t *component)
{
  spawn_state *ss;
  const char **component_names;

  ss = component->ptr;
  component_names = ecs_components_registered ();
  for (int i = 0; component_names[i]; i++)
    {
      ecs_component_destroy (spawn_state_get_component (ss,
                                                        component_names[i]));
    }

  free (component_names);

  free (component->ptr);
}

void
spawn_state_save_entity (ecs_t *ecs,
                         int    entity)
{
  spawn_state *ss;
  const char **component_names;

  ss = ecs_entity_get_component_ptr (ecs, entity, "spawn_state");
  component_names = ecs_entity_get_component_names (ecs, entity);

  for (int j = 0; component_names[j]; j++)
    {
      if (g_str_equal (component_names[j], "spawn_state"))
        continue;

      ecs_component_copy (ecs_entity_get_component (ecs, entity,
                                                    component_names[j]),
                          spawn_state_get_component (ss, component_names[j]));
    }

  free (component_names);
}

void
spawn_state_save_entities (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, spawn_state_sig);
  for (int i = 0; entities[i] >= 0; i++)
    spawn_state_save_entity (ecs, entities[i]);

  free (entities);
}

void
spawn_state_reset_entities (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, spawn_state_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      spawn_state *ss;
      const char **component_names;

      ss = ecs_entity_get_component_ptr (ecs, entities[i], "spawn_state");
      component_names = ecs_entity_get_component_names (ecs, entities[i]);

      for (int j = 0; component_names[j]; j++)
        {
          component_t *component;

          if (g_str_equal (component_names[j], "spawn_state"))
            continue;

          component = ecs_entity_get_component (ecs, entities[i],
                                                component_names[j]);

          ecs_component_destroy (component);
          ecs_component_copy (spawn_state_get_component (ss,
                                                         component_names[j]),
                              component);
        }

      free (component_names);
    }

  free (entities);
}

component_t *
spawn_state_get_component (spawn_state *ss,
                           const char  *component_name)
{
  return &ss->components[ecs_component_get_id (component_name)];
}

void *
spawn_state_get_component_ptr (spawn_state *ss,
                               const char  *component_name)
{
  return spawn_state_get_component (ss, component_name)->ptr;
}
