/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "velocity.h"

#include <assert.h>
#include <math.h>

#include "util/util.h"

void
velocity_decelerate (velocity *v,
                     double    horizontal,
                     double    vertical)
{
  assert (horizontal >= 0 && vertical >= 0);

  if (v->horizontal > 0)
    {
      v->horizontal -= horizontal;
      if (v->horizontal < 0)
        v->horizontal = 0;
    }
  else if (v->horizontal < 0)
    {
      v->horizontal += horizontal;
      if (v->horizontal > 0)
        v->horizontal = 0;
    }

  if (v->vertical > 0)
    {
      v->vertical -= vertical;
      if (v->vertical < 0)
        v->vertical = 0;
    }
  else if (v->vertical < 0)
    {
      v->vertical += vertical;
      if (v->vertical > 0)
        v->vertical = 0;
    }
}

void
velocity_accelerate (velocity *v,
                     double    horizontal,
                     double    vertical)
{
  v->horizontal += horizontal;
  if (fabs (v->horizontal) > v->max_horizontal)
    v->horizontal = SIGN (v->horizontal) * v->max_horizontal;

  v->vertical += vertical;
  if (fabs (v->vertical) > v->max_vertical)
    v->vertical = SIGN (v->vertical) * v->max_vertical;
}
