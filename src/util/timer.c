/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timer.h"

#include <math.h>

#include "constants.h"

static int game_time = 0;

void
timer_init (timer *tmr)
{
  tmr->ticks = -1;
  tmr->start_time = -1;
}

void
timer_start (timer *tmr,
             int    ticks)
{
  tmr->ticks = ticks;
  tmr->start_time = game_time;
}

static bool
timer_started (timer *tmr)
{
  return !(tmr->ticks < 0 || tmr->start_time < 0);
}

int
timer_elapsed_ticks (timer *tmr)
{
  if (!timer_started (tmr))
    return 0;

  return game_time - tmr->start_time;
}

time_ms
timer_elapsed_time (timer *tmr)
{
  int ticks_elapsed, minutes, seconds;
  time_ms tms;

  if (!timer_started (tmr))
    {
      tms.minutes = 0;
      tms.seconds = 0;
      return tms;
    }

  ticks_elapsed = timer_elapsed_ticks (tmr);
  minutes = ticks_elapsed / (FRAMERATE * 60);
  seconds = ticks_elapsed / FRAMERATE - 60 * minutes;

  tms.minutes = minutes;
  tms.seconds = seconds;

  return tms;
}

bool
timer_complete (timer *tmr)
{
  if (!timer_started (tmr))
    return true;

  return game_time >= tmr->start_time + tmr->ticks;
}

double
timer_progress (timer *tmr)
{
  if (!timer_started (tmr) || timer_complete (tmr))
    return 1.0;

  return (double) (game_time - tmr->start_time) / tmr->ticks;
}

void
game_time_update (void)
{
  game_time++;
}
