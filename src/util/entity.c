/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "entity.h"

#include <assert.h>

#include "constants.h"

bool
rects_overlap (ecs_t *ecs,
               int    entity_a,
               int    entity_b)
{
  position *a_pos, *b_pos;
  dimensions *a_dim, *b_dim;

  if (entity_a == entity_b)
    return false;

  a_pos = ecs_entity_get_component_ptr (ecs, entity_a, "position");
  a_dim = ecs_entity_get_component_ptr (ecs, entity_a, "dimensions");
  b_pos = ecs_entity_get_component_ptr (ecs, entity_b, "position");
  b_dim = ecs_entity_get_component_ptr (ecs, entity_b, "dimensions");

  return !(a_pos->x >= b_pos->x + b_dim->width /* a is right of b */
           || a_pos->x + a_dim->width <= b_pos->x /* a is left of b */
           || a_pos->y + a_dim->height <= b_pos->y /* a is above b */
           || a_pos->y >= b_pos->y + b_dim->height); /* a is below b */
}

int
rect_at_pos (ecs_t  *ecs,
             double  x,
             double  y)
{
  int *entities;

  entities = ecs_entities_find (ecs, rect_sig);
  for (int i = 0; entities[i] >= 0; i++)
    {
      position *pos;
      dimensions *dim;

      pos = ecs_entity_get_component_ptr (ecs, entities[i], "position");
      dim = ecs_entity_get_component_ptr (ecs, entities[i], "dimensions");

      if (x >= pos->x
          && y >= pos->y
          && x < pos->x + dim->width
          && y < pos->y + dim->height)
        {
          int entity;

          entity = entities[i];
          free (entities);
          return entity;
        }
    }

  free (entities);
  return -1;
}

bool
rect_is_at_pos (ecs_t  *ecs,
                int     entity,
                double  x,
                double  y)
{
  position *pos;
  dimensions *dim;

  pos = ecs_entity_get_component_ptr (ecs, entity, "position");
  dim = ecs_entity_get_component_ptr (ecs, entity, "dimensions");

  return (x >= pos->x
          && y >= pos->y
          && x < pos->x + dim->width
          && y < pos->y + dim->height);
}

int
player_find (ecs_t *ecs)
{
  int *entities;
  int entity;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("player"));

  assert (entities[1] < 0);

  entity = entities[0];
  free (entities);

  return entity;
}

void
puffer_adjust_starting_velocity (ecs_t *ecs)
{
  int *entities;

  entities = ecs_entities_find (ecs, ecs_entity_type_signature ("puffer"));
  for (int i = 0; entities[i] >= 0; i++)
    {
      puffer *puff;
      velocity *v;

      puff = ecs_entity_get_component_ptr (ecs, entities[i], "puffer");
      v = ecs_entity_get_component_ptr (ecs, entities[i], "velocity");

      if (puff->periods_elapsed == 0)
        v->vertical = 0;
      else
        {
          if (puff->accelerate)
            v->vertical = puff->movement_direction * PUFFER_MIN_SPEED;
          else
            v->vertical = puff->movement_direction * PUFFER_MAX_SPEED;
        }
    }

  free (entities);
}
