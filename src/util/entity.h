/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef UTIL_ENTITY_H
#define UTIL_ENTITY_H

#include "ecs/ecs.h"

bool rects_overlap                   (ecs_t *ecs,
                                      int    entity_a,
                                      int    entity_b);
/* Returns entity found at (x, y) or -1 if no entity is found. */
int  rect_at_pos                     (ecs_t  *ecs,
                                      double  x,
                                      double  y);
bool rect_is_at_pos                  (ecs_t  *ecs,
                                      int     entity,
                                      double  x,
                                      double  y);

int  player_find                     (ecs_t *ecs);

void puffer_adjust_starting_velocity (ecs_t *ecs);

#endif
