/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "input.h"

#include <stdio.h>

#include "constants.h"
#include "settings.h"

void
input_update (Input *input)
{
  const Uint8 *keyboard_state;

  keyboard_state = SDL_GetKeyboardState (NULL);

  input->previous = input->current;

  input->current.up = keyboard_state[SDL_GetScancodeFromKey
                                     (settings_get_bind ("control_up"))];

  input->current.down = keyboard_state[SDL_GetScancodeFromKey
                                       (settings_get_bind ("control_down"))];

  input->current.left = keyboard_state[SDL_GetScancodeFromKey
                                       (settings_get_bind ("control_left"))];

  input->current.right = keyboard_state[SDL_GetScancodeFromKey
                                        (settings_get_bind ("control_right"))];

  input->current.jump = keyboard_state[SDL_GetScancodeFromKey
                                       (settings_get_bind ("control_jump"))];
}

double
to_game_coordinate (const Camera    *camera,
                    double           value,
                    Coordinate_Axis  axis,
                    bool             absolute)
{
  double result;

  switch (axis)
    {
    case X_AXIS:
      result = value * camera->zoom - (camera->zoom * WINDOW_WIDTH) / 2;
      if (absolute)
        result += camera->x;
      break;

    case Y_AXIS:
      result = value * camera->zoom - (camera->zoom * WINDOW_HEIGHT) / 2;
      if (absolute)
        result += camera->y;
      break;

    default:
      fprintf (stderr, "Invalid axis %d in to_game_coordinate.\n", axis);
      return 0;
    }

  return result;
}
