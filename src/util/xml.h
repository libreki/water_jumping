/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef XML_H
#define XML_H

#include <stdbool.h>

#include <libxml/parser.h>
#include <libxml/xmlwriter.h>

xmlDoc        *xml_read_file_or_exit      (const char *filename);

bool           xml_has_prop               (const xmlNode *node,
                                           const char    *name);
/* Caller is responsible for freeing return value. */
char          *xml_get_prop_string        (const xmlNode *node,
                                           const char    *name);
double         xml_get_prop_double        (const xmlNode *node,
                                           const char    *name);
int            xml_get_prop_int           (const xmlNode *node,
                                           const char    *name);
bool           xml_get_prop_bool          (const xmlNode *node,
                                           const char    *name);

bool           xml_element_is             (const xmlNode *node,
                                           const char    *name);
void           xml_assert_element         (const xmlNode *node,
                                           const char    *name);

#define XML_NEXT_OR_BREAK(node)                 \
  if (node->next)                               \
    node = node->next;                          \
  else                                          \
    break;

xmlTextWriter *xml_new_writer             (const char *uri);
void           xml_start_element          (xmlTextWriter *writer,
                                           const char    *name);
void           xml_end_element            (xmlTextWriter *writer);
void           xml_write_attribute        (xmlTextWriter *writer,
                                           const char    *name,
                                           const char    *content);
void           xml_write_attribute_int    (xmlTextWriter *writer,
                                           const char    *name,
                                           int            content);
void           xml_write_attribute_double (xmlTextWriter *writer,
                                           const char    *name,
                                           double         content);
void           xml_write_attribute_bool   (xmlTextWriter *writer,
                                           const char    *name,
                                           bool           content);

#endif
