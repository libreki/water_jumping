/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TIMER_H
#define TIMER_H

#include <stdbool.h>

typedef struct timer
{
  int ticks;
  int start_time;
} timer;

typedef struct time_ms
{
  int minutes;
  int seconds;
} time_ms;

void     timer_init          (timer *tmr);
void     timer_start         (timer *tmr,
                              int    ticks);
int      timer_elapsed_ticks (timer *tmr);
time_ms  timer_elapsed_time  (timer *tmr);
bool     timer_complete      (timer *tmr);
/* Returns value between 0 and 1. */
double   timer_progress      (timer *tmr);

void     game_time_update    (void);

#endif
