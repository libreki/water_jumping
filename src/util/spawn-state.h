/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SPAWN_STATE_H
#define SPAWN_STATE_H

#include "ecs/ecs.h"

void         spawn_state_ptr_copy          (component_t *src,
                                            component_t *dest);
void         spawn_state_ptr_destroy       (component_t *component);

void         spawn_state_save_entity       (ecs_t *ecs,
                                            int    entity);
void         spawn_state_save_entities     (ecs_t *ecs);
void         spawn_state_reset_entities    (ecs_t *ecs);

component_t *spawn_state_get_component     (spawn_state *ss,
                                            const char  *component_name);
void        *spawn_state_get_component_ptr (spawn_state *ss,
                                            const char  *component_name);

#endif
