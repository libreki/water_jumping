/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "entity-menu.h"

#include "video/text.h"

#define TEXT_SIZE 24

void
render_entity_menu (SDL_Renderer *renderer,
                    menu         *entity_menu)
{
  for (int i = 0; i < entity_menu->item_count; i++)
    {
      SDL_Color color;

      if (entity_menu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, entity_menu->items[i],
                   180, i * TEXT_SIZE);
    }
}

void
entity_menu_select_item (menu   *entity_menu,
                         editor *e)
{
  const char **type_names;

  type_names = ecs_entity_type_names ();
  e->creating_entity_type = type_names[entity_menu->selected_item];
  menu_close ();

  free (type_names);
}
