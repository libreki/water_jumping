/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef COMPONENT_MENU_H
#define COMPONENT_MENU_H

#include "menu.h"

typedef struct component_submenu_item
{
  const char *name;
  char       *value;
} component_submenu_item;

void  render_component_menu         (SDL_Renderer *renderer,
                                     menu         *component_menu);
void  render_component_submenu      (SDL_Renderer *renderer,
                                     menu         *submenu);

void  component_submenu_destroy     (menu *submenu);
void *component_submenu_item_new    (const char *name);

void  component_menu_select_item    (menu *component_menu);
void  component_submenu_select_item (menu   *component_submenu,
                                     editor *e);

#endif
