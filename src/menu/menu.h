/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef MENU_H
#define MENU_H

#include <stdbool.h>

#include <SDL.h>

#include "editor/editor.h"
#include "level.h"

typedef enum
{
  MAIN_MENU_T,
  PAUSE_MENU_T,
  SETTINGS_MENU_T,
  ENTITY_MENU_T,
  COMPONENT_MENU_T,
  POSITION_MENU_T,
  DIMENSIONS_MENU_T,
  SCENE_WARP_MENU_T,
  PUFFER_MENU_T,
  TILEMAP_MENU_T,
  CAMERA_MENU_T,
  OVERWORLD_T
} menu_t;

typedef struct menu menu;

struct menu
{
  menu_t   type;

  void   **items;
  int      selected_item;
  int      item_count;

  void (*render) (SDL_Renderer *, menu *);
  void (*up)     (menu *);
  void (*down)   (menu *);
  void (*left)   (menu *);
  void (*right)  (menu *);
};

void       render_menu                (SDL_Renderer *renderer);
SDL_FPoint menu_overworld_center      (void);

void       menu_global_init           (void);
void       menu_global_destroy        (void);

bool       menu_is_open               (void);
bool       menu_editor_menu_is_open   (void);
bool       menu_stack_head_is         (menu_t mt);

void       menu_open                  (menu_t mt);
void       menu_close                 (void);
void       menu_close_all             (void);

void       menu_update_component_menu (editor *e,
                                       ecs_t  *ecs);

void       menu_pause_pressed         (void);
void       menu_up                    (void);
void       menu_down                  (void);
void       menu_left                  (void);
void       menu_right                 (void);
void       menu_select_item           (level  *lvl,
                                       editor *e);

#endif
