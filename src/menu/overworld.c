/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "overworld.h"

#include "util/xml.h"
#include "video/graphics.h"

static void overworld_next_world     (menu *m);
static void overworld_previous_world (menu *m);
static void overworld_next_level     (menu *m);
static void overworld_previous_level (menu *m);

void
render_overworld (SDL_Renderer *renderer,
                  menu         *m)
{
  overworld *ow;
  int item_index;
  const Texture *background, *uncleared, *unlocked;

  ow = (overworld *) m;

  background = graphics_get_texture ("overworld_background");
  uncleared = graphics_get_texture ("overworld_uncleared_level");
  unlocked = graphics_get_texture ("overworld_unlocked_level");

  SDL_RenderCopy (renderer, (SDL_Texture *) background->atlas,
                  &background->src, NULL);

  item_index = 0;
  for (int i = 0; i < ow->world_count; i++)
    {
      world *w;

      w = &ow->worlds[i];
      for (int j = 0; j < w->level_count; j++)
        {
          level_node *ln;
          SDL_Rect dest;

          ln = &w->levels[j];

          dest.x = ln->x;
          dest.y = ln->y;
          dest.w = uncleared->src.w;
          dest.h = uncleared->src.h;

          if (item_index == ow->parent.selected_item)
            {
              SDL_RenderCopy (renderer, (SDL_Texture *) uncleared->atlas,
                              &uncleared->src, &dest);
            }
          else
            {
              SDL_RenderCopy (renderer, (SDL_Texture *) unlocked->atlas,
                              &unlocked->src, &dest);
            }

          item_index++;
        }
    }
}

SDL_FPoint
overworld_center (overworld *ow)
{
  world *w, *prev_w;
  double progress;
  SDL_FPoint center;

  w = &ow->worlds[ow->current_world];
  prev_w = &ow->worlds[ow->previous_world];

  progress = timer_progress (&ow->scroll_timer);
  center.x = prev_w->x + progress * (w->x - prev_w->x);
  center.y = prev_w->y + progress * (w->y - prev_w->y);

  return center;
}

#define SCROLL_TIME 30

static void
overworld_begin_scroll (overworld *ow)
{
  timer_start (&ow->scroll_timer, SCROLL_TIME);
}

static bool
overworld_is_scrolling (overworld *ow)
{
  return !timer_complete (&ow->scroll_timer);
}

void
overworld_init (overworld *ow)
{
  xmlDoc *doc;
  xmlNode *node;
  int i;

  doc = xml_read_file_or_exit ("./data/level/overworld.xml");
  node = xmlDocGetRootElement (doc);
  xml_assert_element (node, "overworld");

  ow->parent.type = OVERWORLD_T;
  ow->parent.items = NULL;
  ow->parent.selected_item = 0;
  ow->parent.item_count = 0;
  ow->parent.render = render_overworld;
  ow->parent.up = overworld_previous_world;
  ow->parent.down = overworld_next_world;
  ow->parent.left = overworld_previous_level;
  ow->parent.right = overworld_next_level;

  ow->current_world = 0;
  ow->previous_world = 0;
  ow->world_count = xmlChildElementCount (node);
  ow->worlds = malloc (ow->world_count * sizeof (world));
  timer_init (&ow->scroll_timer);

  i = 0;
  node = node->children;
  while (true)
    {
      world *w;
      int j;

      xml_assert_element (node, "world");

      w = &ow->worlds[i];
      w->level_count = xmlChildElementCount (node);
      w->levels = malloc (w->level_count * sizeof (level_node));
      w->x = xml_get_prop_double (node, "x");
      w->y = xml_get_prop_double (node, "y");

      ow->parent.item_count += w->level_count;

      j = 0;
      node = node->children;
      while (true)
        {
          level_node *ln;

          xml_assert_element (node, "level");

          ln = &w->levels[j];
          ln->level_name = xml_get_prop_string (node, "name");
          ln->x = xml_get_prop_int (node, "x");
          ln->y = xml_get_prop_int (node, "y");

          j++;
          XML_NEXT_OR_BREAK (node);
        }
      node = node->parent;

      i++;
      XML_NEXT_OR_BREAK (node);
    }

  xmlFreeDoc (doc);
}

void
overworld_destroy (overworld *ow)
{
  for (int i = 0; i < ow->world_count; i++)
    {
      world *w;

      w = &ow->worlds[i];
      for (int j = 0; j < w->level_count; j++)
        free (w->levels[j].level_name);

      free (w->levels);
    }

  free (ow->worlds);
}

static int
overworld_world_index (overworld *ow)
{
  int n;

  n = 0;
  for (int i = 0; i < ow->world_count; i++)
    {
      n += ow->worlds[i].level_count;
      if (ow->parent.selected_item < n)
        return i;
    }

  fprintf (stderr, "Invalid selected_item %d in overworld.\n",
           ow->parent.selected_item);
  return -1;
}

static void
overworld_next_world (menu *m)
{
  overworld *ow;
  int n;

  ow = (overworld *) m;
  if (overworld_is_scrolling (ow) || ow->current_world >= ow->world_count - 1)
    return;

  n = 0;
  for (int i = 0; i <= ow->current_world; i++)
    n += ow->worlds[i].level_count;

  ow->parent.selected_item = n;
  ow->previous_world = ow->current_world;
  ow->current_world++;

  overworld_begin_scroll (ow);
}

static void
overworld_previous_world (menu *m)
{
  overworld *ow;
  int n;

  ow = (overworld *) m;
  if (overworld_is_scrolling (ow) || ow->current_world <= 0)
    return;

  n = 0;
  for (int i = 0; i < ow->current_world; i++)
    n += ow->worlds[i].level_count;

  ow->parent.selected_item = n - 1;
  ow->previous_world = ow->current_world;
  ow->current_world--;

  overworld_begin_scroll (ow);
}

static void
overworld_next_level (menu *m)
{
  overworld *ow;
  int world_index;

  ow = (overworld *) m;
  if (overworld_is_scrolling (ow))
    return;

  ow->parent.selected_item++;
  if (ow->parent.selected_item >= ow->parent.item_count)
    {
      ow->parent.selected_item = ow->parent.item_count - 1;
      return;
    }

  world_index = overworld_world_index (ow);
  if (world_index != ow->current_world)
    {
      ow->previous_world = ow->current_world;
      ow->current_world = world_index;

      overworld_begin_scroll (ow);
    }
}

static void
overworld_previous_level (menu *m)
{
  overworld *ow;
  int world_index;

  ow = (overworld *) m;
  if (overworld_is_scrolling (ow))
    return;

  ow->parent.selected_item--;
  if (ow->parent.selected_item < 0)
    {
      ow->parent.selected_item = 0;
      return;
    }

  world_index = overworld_world_index (ow);
  if (world_index != ow->current_world)
    {
      ow->previous_world = ow->current_world;
      ow->current_world = world_index;

      overworld_begin_scroll (ow);
    }
}

void
overworld_select_item (overworld *ow,
                       level     *lvl)
{
  int item_index;

  if (overworld_is_scrolling (ow))
    return;

  item_index = 0;
  for (int i = 0; i < ow->world_count; i++)
    {
      world *w;

      w = &ow->worlds[i];
      for (int j = 0; j < w->level_count; j++)
        {
          if (item_index == ow->parent.selected_item)
            {
              level_init (lvl, w->levels[j].level_name);
              menu_close_all ();
              return;
            }

          item_index++;
        }
    }

  fprintf (stderr, "Invalid selected_item %d in overworld.\n",
           ow->parent.selected_item);
}
