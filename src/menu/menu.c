/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "menu.h"

#include <stdio.h>

#include <glib.h>

#include "ecs/ecs.h"
#include "component-menu.h"
#include "entity-menu.h"
#include "main-menu.h"
#include "overworld.h"
#include "pause-menu.h"
#include "settings-menu.h"
#include "video/tilemap.h"

static GQueue *menu_stack = NULL;

static menu main_menu;
static menu pause_menu;
static menu settings_menu;
static menu entity_menu;
static menu component_menu;

static menu position_menu;
static menu dimensions_menu;
static menu scene_warp_menu;
static menu puffer_menu;
static menu tilemap_menu;
static menu camera_menu;

static overworld ow;

#define MENU_STACK_HEAD_OR_RETURN(m)                    \
  if (g_queue_is_empty (menu_stack))                    \
    return;                                             \
  m = g_queue_peek_head (menu_stack);

void
render_menu (SDL_Renderer *renderer)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);
  m->render (renderer, m);
}

SDL_FPoint
menu_overworld_center (void)
{
  return overworld_center (&ow);
}

static void
menu_previous_item (menu *m)
{
  m->selected_item--;
  if (m->selected_item < 0)
    m->selected_item = m->item_count - 1;
}

static void
menu_next_item (menu *m)
{
  m->selected_item++;
  if (m->selected_item >= m->item_count)
    m->selected_item = 0;
}

static void
menu_standard_init (menu   *m,
                    menu_t  type,
                    void (*render) (SDL_Renderer *, menu *),
                    int     item_count,
                    ...)
{
  va_list ap;

  m->type = type;

  m->selected_item = 0;
  m->item_count = item_count;

  m->render = render;
  m->up = menu_previous_item;
  m->down = menu_next_item;
  m->left = NULL;
  m->right = NULL;

  if (item_count <= 0)
    {
      m->items = NULL;
      return;
    }

  m->items = malloc (item_count * sizeof (char *));
  va_start (ap, item_count);
  for (int i = 0; i < item_count; i++)
    m->items[i] = va_arg (ap, char *);

  va_end (ap);
}

static void
menu_standard_destroy (menu *m)
{
  free (m->items);
}

void
menu_global_init (void)
{
  menu_stack = g_queue_new ();

  menu_standard_init (&main_menu, MAIN_MENU_T,
                      render_main_menu, MAIN_MENU_ITEM_N,
                      "Start", "Editor", "Settings", "Quit");

  menu_standard_init (&pause_menu, PAUSE_MENU_T,
                      render_pause_menu, PAUSE_MENU_ITEM_N,
                      "Continue", "Settings", "Quit to main menu", "Quit");

  menu_standard_init (&settings_menu, SETTINGS_MENU_T,
                      render_settings_menu, SETTINGS_MENU_ITEM_N,
                      "Quit", "Reset", "Up", "Down", "Left", "Right",
                      "Jump/Select", "Pause/Cancel");

  menu_standard_init (&entity_menu, ENTITY_MENU_T,
                      render_entity_menu, 0);
  entity_menu.items = (void **) ecs_entity_type_names ();
  entity_menu.item_count = ecs_entity_type_count ();

  menu_standard_init (&component_menu, COMPONENT_MENU_T,
                      render_component_menu, 0);

  menu_standard_init (&position_menu, POSITION_MENU_T,
                      render_component_submenu, 2,
                      component_submenu_item_new ("x"),
                      component_submenu_item_new ("y"));

  menu_standard_init (&dimensions_menu, DIMENSIONS_MENU_T,
                      render_component_submenu, 2,
                      component_submenu_item_new ("width"),
                      component_submenu_item_new ("height"));

  menu_standard_init (&scene_warp_menu, SCENE_WARP_MENU_T,
                      render_component_submenu, 3,
                      component_submenu_item_new ("scene_index"),
                      component_submenu_item_new ("player_x"),
                      component_submenu_item_new ("player_y"));

  menu_standard_init (&puffer_menu, PUFFER_MENU_T,
                      render_component_submenu, 3,
                      component_submenu_item_new ("periods_elapsed"),
                      component_submenu_item_new ("movement_direction"),
                      component_submenu_item_new ("accelerate"));

  menu_standard_init (&tilemap_menu, TILEMAP_MENU_T,
                      render_component_submenu, 1,
                      component_submenu_item_new ("tileset"));

  menu_standard_init (&camera_menu, CAMERA_MENU_T,
                      render_component_submenu, 4,
                      component_submenu_item_new ("center_x"),
                      component_submenu_item_new ("center_y"),
                      component_submenu_item_new ("zoom"),
                      component_submenu_item_new ("threshold"));

  overworld_init (&ow);

  g_queue_push_head (menu_stack, &main_menu);
}

void
menu_global_destroy (void)
{
  g_queue_free (menu_stack);

  menu_standard_destroy (&main_menu);

  menu_standard_destroy (&pause_menu);

  menu_standard_destroy (&settings_menu);

  menu_standard_destroy (&entity_menu);

  component_submenu_destroy (&position_menu);
  component_submenu_destroy (&dimensions_menu);
  component_submenu_destroy (&scene_warp_menu);
  component_submenu_destroy (&puffer_menu);
  component_submenu_destroy (&tilemap_menu);
  component_submenu_destroy (&camera_menu);

  overworld_destroy (&ow);
}

bool
menu_is_open (void)
{
  return !g_queue_is_empty (menu_stack);
}

bool
menu_editor_menu_is_open (void)
{
  return (menu_stack_head_is (ENTITY_MENU_T)
          || menu_stack_head_is (COMPONENT_MENU_T)
          || menu_stack_head_is (POSITION_MENU_T)
          || menu_stack_head_is (DIMENSIONS_MENU_T)
          || menu_stack_head_is (SCENE_WARP_MENU_T)
          || menu_stack_head_is (PUFFER_MENU_T)
          || menu_stack_head_is (TILEMAP_MENU_T)
          || menu_stack_head_is (CAMERA_MENU_T));
}

bool
menu_stack_head_is (menu_t mt)
{
  return (menu_is_open ()
          && ((menu *) g_queue_peek_head (menu_stack))->type == mt);
}

void
menu_open (menu_t mt)
{
  switch (mt)
    {
    case MAIN_MENU_T:
      menu_close_all ();
      g_queue_push_head (menu_stack, &main_menu);
      break;

    case PAUSE_MENU_T:
      g_queue_push_head (menu_stack, &pause_menu);
      break;

    case SETTINGS_MENU_T:
      g_queue_push_head (menu_stack, &settings_menu);
      break;

    case ENTITY_MENU_T:
      if (!menu_is_open ())
        g_queue_push_head (menu_stack, &entity_menu);
      break;

    case COMPONENT_MENU_T:
      if (!menu_is_open () && component_menu.item_count > 0)
        g_queue_push_head (menu_stack, &component_menu);
      break;

    case POSITION_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &position_menu);
      break;

    case DIMENSIONS_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &dimensions_menu);
      break;

    case SCENE_WARP_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &scene_warp_menu);
      break;

    case PUFFER_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &puffer_menu);
      break;

    case TILEMAP_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &tilemap_menu);
      break;

    case CAMERA_MENU_T:
      if (menu_stack_head_is (COMPONENT_MENU_T))
        g_queue_push_head (menu_stack, &camera_menu);
      break;

    case OVERWORLD_T:
      menu_open (MAIN_MENU_T);
      g_queue_push_head (menu_stack, &ow);
      break;

    default:
      fprintf (stderr, "MENU ERROR: Menu of unknown type %d.\n", mt);
      break;
    }
}

void
menu_close (void)
{
  g_queue_pop_head (menu_stack);
}

void
menu_close_all (void)
{
  g_queue_clear (menu_stack);
}

/* TODO: Cleanup */
void
menu_update_component_menu (editor *e,
                            ecs_t  *ecs)
{
  const char *type_name;
  const char **vars;
  component_submenu_item *item;

  if (e->selected_entity < 0)
    {
      component_menu.items = NULL;
      component_menu.item_count = 0;
      return;
    }

  /* TODO: What if entity is not of any type */
  type_name = ecs_entity_find_type (ecs, e->selected_entity);

  vars = ecs_entity_type_vars (type_name);
  component_menu.items = (void **) vars;
  component_menu.selected_item = 0;
  component_menu.item_count = ecs_entity_type_var_count (type_name);

  for (int i = 0; vars[i]; i++)
    {
      if (g_str_equal (vars[i], "position"))
        {
          position *pos;

          pos = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                              "position");
          for (int j = 0; j < position_menu.item_count; j++)
            {
              item = position_menu.items[j];
              if (g_str_equal (item->name, "x"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", pos->x);
                }
              else if (g_str_equal (item->name, "y"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", pos->y);
                }
            }
        }
      else if (g_str_equal (vars[i], "dimensions"))
        {
          dimensions *dim;

          dim = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                              "dimensions");
          for (int j = 0; j < dimensions_menu.item_count; j++)
            {
              item = dimensions_menu.items[j];
              if (g_str_equal (item->name, "width"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d", dim->width);
                }
              else if (g_str_equal (item->name, "height"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d", dim->height);
                }
            }
        }
      else if (g_str_equal (vars[i], "scene_warp"))
        {
          scene_warp *sw;

          sw = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                             "scene_warp");
          for (int j = 0; j < scene_warp_menu.item_count; j++)
            {
              item = scene_warp_menu.items[j];
              if (g_str_equal (item->name, "scene_index"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d", sw->scene_index);
                }
              else if (g_str_equal (item->name, "player_x"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", sw->player_y);
                }
              else if (g_str_equal (item->name, "player_y"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", sw->player_y);
                }
            }
        }
      else if (g_str_equal (vars[i], "puffer"))
        {
          puffer *puff;

          puff = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                               "puffer");
          for (int j = 0; j < puffer_menu.item_count; j++)
            {
              item = puffer_menu.items[j];
              if (g_str_equal (item->name, "periods_elapsed"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d", puff->periods_elapsed);
                }
              else if (g_str_equal (item->name, "movement_direction"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d",
                                                 puff->movement_direction);
                }
              else if (g_str_equal (item->name, "accelerate"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%d", puff->accelerate);
                }
            }
        }
      else if (g_str_equal (vars[i], "tilemap"))
        {
          Tilemap *tilemap;

          tilemap = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                                  "tilemap");
          for (int j = 0; j < tilemap_menu.item_count; j++)
            {
              item = tilemap_menu.items[j];
              if (g_str_equal (item->name, "tileset"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup (tilemap->tileset);
                }
            }
        }
      else if (g_str_equal (vars[i], "camera"))
        {
          camera *cam;

          cam = ecs_entity_get_component_ptr (ecs, e->selected_entity,
                                              "camera");
          for (int j = 0; j < camera_menu.item_count; j++)
            {
              item = camera_menu.items[j];
              if (g_str_equal (item->name, "center_x"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", cam->center_x);
                }
              else if (g_str_equal (item->name, "center_y"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", cam->center_y);
                }
              else if (g_str_equal (item->name, "zoom"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", cam->zoom);
                }
              else if (g_str_equal (item->name, "threshold"))
                {
                  if (item->value)
                    free (item->value);

                  item->value = g_strdup_printf ("%f", cam->threshold);
                }
            }
        }
    }
}

void
menu_pause_pressed (void)
{
  if (!menu_is_open ())
    menu_open (PAUSE_MENU_T);
  else
    {
      if (!menu_stack_head_is (MAIN_MENU_T))
        menu_close ();
    }
}

void
menu_up (void)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);
  if (m->up)
    m->up (m);
}

void
menu_down (void)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);
  if (m->down)
    m->down (m);
}

void
menu_left (void)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);
  if (m->left)
    m->left (m);
}

void
menu_right (void)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);
  if (m->right)
    m->right (m);
}

void
menu_select_item (level  *lvl,
                  editor *e)
{
  menu *m;

  MENU_STACK_HEAD_OR_RETURN (m);

  switch (m->type)
    {
    case MAIN_MENU_T:
      main_menu_select_item (&main_menu, e);
      break;

    case PAUSE_MENU_T:
      pause_menu_select_item (&pause_menu, lvl, e);
      break;

    case SETTINGS_MENU_T:
      settings_menu_select_item (&settings_menu);
      break;

    case ENTITY_MENU_T:
      entity_menu_select_item (&entity_menu, e);
      break;

    case COMPONENT_MENU_T:
      component_menu_select_item (&component_menu);
      break;

    case POSITION_MENU_T:
      component_submenu_select_item (&position_menu, e);
      break;

    case DIMENSIONS_MENU_T:
      component_submenu_select_item (&dimensions_menu, e);
      break;

    case SCENE_WARP_MENU_T:
      component_submenu_select_item (&scene_warp_menu, e);
      break;

    case PUFFER_MENU_T:
      component_submenu_select_item (&puffer_menu, e);
      break;

    case TILEMAP_MENU_T:
      component_submenu_select_item (&tilemap_menu, e);
      break;

    case CAMERA_MENU_T:
      component_submenu_select_item (&camera_menu, e);
      break;

    case OVERWORLD_T:
      overworld_select_item (&ow, lvl);
      break;

    default:
      fprintf (stderr, "MENU ERROR: Menu of unknown type %d.\n", m->type);
      break;
    }
}
