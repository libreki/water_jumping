/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "settings-menu.h"

#include <glib.h>

#include "settings.h"
#include "video/text.h"

#define TEXT_SIZE 72
#define BIND_X 640

static const char *control_names[6] = { "control_up", "control_down",
                                        "control_left", "control_right",
                                        "control_jump", "control_pause" };

void
render_settings_menu (SDL_Renderer *renderer,
                      menu         *settings_menu)
{
  for (int i = 0; i < settings_menu->item_count; i++)
    {
      SDL_Color color;

      if (settings_menu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, settings_menu->items[i],
                   0, i * TEXT_SIZE);

      if (i >= 2)
        {
          SDL_Keycode bind;

          bind = settings_get_bind (control_names[i - 2]);

          render_text (renderer, "LiberationMono-Regular.ttf",
                       TEXT_SIZE, color, SDL_GetKeyName (bind),
                       BIND_X, i * TEXT_SIZE);
        }
    }
}

void
settings_menu_select_item (menu *settings_menu)
{
  switch ((settings_menu_item) settings_menu->selected_item)
    {
    case SETTINGS_MENU_QUIT:
      menu_close ();
      break;

    case SETTINGS_MENU_RESET:
      settings_reset ();
      break;

    case SETTINGS_MENU_UP:
      settings_begin_bind_change ("control_up");
      break;

    case SETTINGS_MENU_DOWN:
      settings_begin_bind_change ("control_right");
      break;

    case SETTINGS_MENU_LEFT:
      settings_begin_bind_change ("control_left");
      break;

    case SETTINGS_MENU_RIGHT:
      settings_begin_bind_change ("control_right");
      break;

    case SETTINGS_MENU_JUMP:
      settings_begin_bind_change ("control_jump");
      break;

    case SETTINGS_MENU_PAUSE:
      settings_begin_bind_change ("control_pause");
      break;

    default:
      fprintf (stderr, "MENU ERROR: Unknown settings menu item %d.\n",
               settings_menu->selected_item);
      break;
    }
}
