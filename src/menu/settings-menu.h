/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SETTINGS_MENU_H
#define SETTINGS_MENU_H

#include "menu.h"

typedef enum
{
  SETTINGS_MENU_QUIT = 0,
  SETTINGS_MENU_RESET,
  SETTINGS_MENU_UP,
  SETTINGS_MENU_DOWN,
  SETTINGS_MENU_LEFT,
  SETTINGS_MENU_RIGHT,
  SETTINGS_MENU_JUMP,
  SETTINGS_MENU_PAUSE,
  SETTINGS_MENU_ITEM_N
} settings_menu_item;

void render_settings_menu         (SDL_Renderer *renderer,
                                   menu         *settings_menu);

void settings_menu_select_item    (menu *settings_menu);

#endif
