/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef OVERWORLD_H
#define OVERWORLD_H

#include "menu.h"
#include "util/timer.h"

typedef struct level_node
{
  double  x;
  double  y;
  char   *level_name;
} level_node;

typedef struct world
{
  double      x;
  double      y;
  level_node *levels;
  int         level_count;
} world;

typedef struct overworld
{
  menu   parent;

  world *worlds;
  int    current_world;
  int    previous_world;
  int    world_count;

  timer  scroll_timer;
} overworld;

void       render_overworld      (SDL_Renderer *renderer,
                                  menu         *m);
SDL_FPoint overworld_center      (overworld *ow);

void       overworld_init        (overworld *ow);
void       overworld_destroy     (overworld *ow);

void       overworld_select_item (overworld *ow,
                                  level     *lvl);

#endif
