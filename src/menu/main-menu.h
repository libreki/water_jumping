/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "editor/editor.h"
#include "menu.h"

typedef enum
{
  MAIN_MENU_START = 0,
  MAIN_MENU_EDITOR,
  MAIN_MENU_SETTINGS,
  MAIN_MENU_QUIT,
  MAIN_MENU_ITEM_N
} main_menu_item;

void render_main_menu      (SDL_Renderer *renderer,
                            menu         *main_menu);

void main_menu_select_item (menu   *main_menu,
                            editor *e);

#endif
