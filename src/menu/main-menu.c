/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "main-menu.h"

#include "constants.h"
#include "video/graphics.h"
#include "video/text.h"

#define TEXT_SIZE 72
#define LICENSE_TEXT_SIZE 17
#define GPL_LOGO_WIDTH 150
#define GPL_LOGO_HEIGHT 63

void
render_main_menu (SDL_Renderer *renderer,
                  menu         *main_menu)
{
  const Texture *gplv3;
  SDL_Rect gplv3_dest;

  for (int i = 0; i < main_menu->item_count; i++)
    {
      SDL_Color color;

      if (main_menu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, main_menu->items[i],
                   0, i * TEXT_SIZE);
    }

  gplv3 = graphics_get_texture ("gplv3");

  gplv3_dest.x = 0;
  gplv3_dest.y = WINDOW_HEIGHT - GPL_LOGO_HEIGHT;
  gplv3_dest.w = GPL_LOGO_WIDTH;
  gplv3_dest.h = GPL_LOGO_HEIGHT;

  SDL_RenderCopy (renderer, (SDL_Texture *) gplv3->atlas,
                  &gplv3->src, &gplv3_dest);

  render_text (renderer, "LiberationMono-Regular.ttf",
               LICENSE_TEXT_SIZE, (SDL_Color) { 255, 255, 255, 255 },
               "[Title] Copyright (C) 2022-2023 libreki",
               GPL_LOGO_WIDTH, WINDOW_HEIGHT - GPL_LOGO_HEIGHT);

  render_text (renderer, "LiberationMono-Regular.ttf",
               LICENSE_TEXT_SIZE, (SDL_Color) { 255, 255, 255, 255 },
               "This program comes with ABSOLUTELY NO WARRANTY. This is free "
               "software, and you are",
               GPL_LOGO_WIDTH, WINDOW_HEIGHT - 2 * GPL_LOGO_HEIGHT / 3);

  render_text (renderer, "LiberationMono-Regular.ttf",
               LICENSE_TEXT_SIZE, (SDL_Color) { 255, 255, 255, 255 },
               "welcome to redistribute it under certain conditions; see the"
               "GNU GPL for details.",
               GPL_LOGO_WIDTH, WINDOW_HEIGHT - GPL_LOGO_HEIGHT / 3);
}

void
main_menu_select_item (menu   *main_menu,
                       editor *e)
{
  SDL_QuitEvent event;
  event.type = SDL_QUIT;

  switch ((main_menu_item) main_menu->selected_item)
    {
    case MAIN_MENU_START:
      menu_open (OVERWORLD_T);
      break;

    case MAIN_MENU_EDITOR:
      menu_close_all ();
      editor_start (e);
      break;

    case MAIN_MENU_SETTINGS:
      menu_open (SETTINGS_MENU_T);
      break;

    case MAIN_MENU_QUIT:
      SDL_PushEvent ((SDL_Event *) &event);
      break;

    default:
      fprintf (stderr, "MENU ERROR: Unknown main menu item %d.\n",
               main_menu->selected_item);
      break;
    }
}
