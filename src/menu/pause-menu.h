/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PAUSE_MENU_H
#define PAUSE_MENU_H

#include "menu.h"

typedef enum
{
  PAUSE_MENU_CONTINUE = 0,
  PAUSE_MENU_SETTINGS,
  PAUSE_MENU_QUIT_TO_MAIN_MENU,
  PAUSE_MENU_QUIT,
  PAUSE_MENU_ITEM_N
} pause_menu_item;

void render_pause_menu      (SDL_Renderer *renderer,
                             menu         *pause_menu);

void pause_menu_select_item (menu   *pause_menu,
                             level  *lvl,
                             editor *e);

#endif
