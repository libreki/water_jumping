/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pause-menu.h"

#include "video/text.h"

#define TEXT_SIZE 72

void
render_pause_menu (SDL_Renderer *renderer,
                   menu         *pause_menu)
{
  for (int i = 0; i < pause_menu->item_count; i++)
    {
      SDL_Color color;

      if (pause_menu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, pause_menu->items[i],
                   0, i * TEXT_SIZE);
    }
}

void
pause_menu_select_item (menu   *pause_menu,
                        level  *lvl,
                        editor *e)
{
  SDL_QuitEvent event;
  event.type = SDL_QUIT;

  switch ((pause_menu_item) pause_menu->selected_item)
    {
    case PAUSE_MENU_CONTINUE:
      menu_close ();
      break;

    case PAUSE_MENU_SETTINGS:
      menu_open (SETTINGS_MENU_T);
      break;

    case PAUSE_MENU_QUIT_TO_MAIN_MENU:
      level_destroy (lvl);
      menu_open (MAIN_MENU_T);
      if (e->active)
        editor_stop (e);
      break;

    case PAUSE_MENU_QUIT:
      SDL_PushEvent ((SDL_Event *) &event);
      break;

    default:
      fprintf (stderr, "MENU ERROR: Unknown pause menu item %d.\n",
               pause_menu->selected_item);
      break;
    }
}
