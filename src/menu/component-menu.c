/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "component-menu.h"

#include "video/text.h"

#define TEXT_SIZE 24

void
render_component_menu (SDL_Renderer *renderer,
                       menu         *component_menu)
{
  for (int i = 0; i < component_menu->item_count; i++)
    {
      SDL_Color color;

      if (component_menu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, component_menu->items[i],
                   0, (i + 3) * TEXT_SIZE);
    }
}

void
render_component_submenu (SDL_Renderer *renderer,
                          menu         *submenu)
{
  for (int i = 0; i < submenu->item_count; i++)
    {
      SDL_Color color;
      component_submenu_item *item;

      item = submenu->items[i];

      if (submenu->selected_item == i)
        color = (SDL_Color) { 255, 0, 0, 255 };
      else
        color = (SDL_Color) { 255, 255, 255, 255 };

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, item->name,
                   0, (i + 3) * TEXT_SIZE);

      render_text (renderer, "LiberationMono-Regular.ttf",
                   TEXT_SIZE, color, item->value,
                   360, (i + 3) * TEXT_SIZE);
    }
}

void
component_submenu_destroy (menu *submenu)
{
  for (int i = 0; i < submenu->item_count; i++)
    {
      component_submenu_item *item;

      item = submenu->items[i];
      if (item->value)
        free (item->value);

      free (item);
    }

  free (submenu->items);
}

void *
component_submenu_item_new (const char *name)
{
  component_submenu_item *item;

  item = malloc (sizeof (component_submenu_item));
  item->name = name;
  item->value = NULL;

  return item;
}

void
component_menu_select_item (menu *component_menu)
{
  const char *item;

  item = component_menu->items[component_menu->selected_item];

  if (g_str_equal (item, "position"))
    menu_open (POSITION_MENU_T);
  else if (g_str_equal (item, "dimensions"))
    menu_open (DIMENSIONS_MENU_T);
  else if (g_str_equal (item, "scene_warp"))
    menu_open (SCENE_WARP_MENU_T);
  else if (g_str_equal (item, "puffer"))
    menu_open (PUFFER_MENU_T);
  else if (g_str_equal (item, "tilemap"))
    menu_open (TILEMAP_MENU_T);
  else if (g_str_equal (item, "camera"))
    menu_open (CAMERA_MENU_T);
}

void
component_submenu_select_item (menu   *component_submenu,
                               editor *e)
{
  component_submenu_item *item;

  item = component_submenu->items[component_submenu->selected_item];
  editor_text_entry_activate (e, item->name);
}
