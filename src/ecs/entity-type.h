/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ENTITY_TYPE_H
#define ENTITY_TYPE_H

#include <stdint.h>

#include "decl.h"
#include "util/xml.h"

typedef struct entity_type
{
  /* NULL terminated. */
  char     *vars[MAX_COMPONENTS + 1];

  uint64_t  signature;
} entity_type;

void         ecs_entity_types_global_init    (void);
void         ecs_entity_types_global_destroy (void);

/* Returns ID of new entity. */
int          ecs_entity_of_type_new          (ecs_t      *ecs,
                                              const char *type_name,
                                              xmlNode    *var_node);

void         ecs_entity_of_type_xml_write    (ecs_t         *ecs,
                                              int            entity,
                                              xmlTextWriter *writer,
                                              const char    *type_name);

/* Returns sorted NULL-terminated array of strings. Caller is responsible for
   freeing array but not strings. */
const char **ecs_entity_type_names           (void);

/* Returns the amount of registered entity types. */
int          ecs_entity_type_count           (void);

uint64_t     ecs_entity_type_signature       (const char *type_name);

/* Returns NULL-terminated array of strings. Data must not be freed by
   caller. */
const char **ecs_entity_type_vars            (const char *type_name);
int          ecs_entity_type_var_count       (const char *type_name);
bool         ecs_entity_is_of_type           (ecs_t      *ecs,
                                              int         entity,
                                              const char *type_name);

/* Returns name of entity type or NULL if entity doesn't match any type. */
const char  *ecs_entity_find_type            (ecs_t *ecs,
                                              int    entity);

#endif
