/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SIGNATURES_H
#define SIGNATURES_H

#include <stdint.h>

#include <glib.h>

/* TODO: Don't include all signatures in this file? */
#define SIGNATURES(type)                        \
  type movable_sig;                             \
  type gravity_sig;                             \
  type collidable_sig;                          \
  type obstacle_sig;                            \
  type renderable_sig;                          \
  type tilemap_sig;                             \
  type rect_sig;                                \
  type danger_sig;                              \
  type spawn_state_sig;                         \
  type editor_renderable_sig;

SIGNATURES (extern uint64_t);

void ecs_signatures_calculate (void);

#endif
