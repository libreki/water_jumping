/*  Copyright (C) 2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "entity-type.h"

#include <assert.h>

#include "ecs.h"

/* Keys are string literals, values are entity_type pointers. */
static GHashTable *entity_types;

static void
ecs_entity_type_register (const char *type_name)
{
  entity_type *type;
  int i;
  xmlDoc *doc;
  xmlNode *node;
  char *filename;

  type = malloc (sizeof (entity_type));
  type->signature = 0;

  filename = g_strdup_printf ("./data/entity/%s.xml", type_name);
  doc = xml_read_file_or_exit (filename);
  node = xmlDocGetRootElement (doc);

  xml_assert_element (node, "entity");

  i = 0;
  node = node->children;
  while (true)
    {
      char *component_name;

      xml_assert_element (node, "component");

      component_name = xml_get_prop_string (node, "name");
      type->signature |= 1 << ecs_component_get_id (component_name);

      if (xml_has_prop (node, "var"))
        {
          type->vars[i] = component_name;
          i++;
        }
      else
        free (component_name);

      XML_NEXT_OR_BREAK (node);
    }

  free (filename);
  xmlFreeDoc (doc);

  type->vars[i] = NULL;
  g_hash_table_insert (entity_types, (char *) type_name, type);
}

static void
entity_type_free (entity_type *type)
{
  for (int i = 0; type->vars[i]; i++)
    free (type->vars[i]);

  free (type);
}

void
ecs_entity_types_global_init (void)
{
  entity_types =
    g_hash_table_new_full (g_str_hash, g_str_equal,
                           NULL, (void (*) (void *)) entity_type_free);

  ecs_entity_type_register ("camera");
  ecs_entity_type_register ("checkpoint");
  ecs_entity_type_register ("ground");
  ecs_entity_type_register ("level_end");
  ecs_entity_type_register ("player");
  ecs_entity_type_register ("puffer");
  ecs_entity_type_register ("scene_warp");
  ecs_entity_type_register ("spikes");
  ecs_entity_type_register ("water");
}

void
ecs_entity_types_global_destroy (void)
{
  g_hash_table_destroy (entity_types);
}

int
ecs_entity_of_type_new (ecs_t      *ecs,
                        const char *type_name,
                        xmlNode    *var_node)
{
  int entity;
  xmlDoc *doc;
  xmlNode *node;
  GHashTable *var_components;
  char *filename;

  entity = ecs_entity_new (ecs);

  /* Construct variable components if not NULL */

  if (var_node)
    {
      var_components = g_hash_table_new_full (g_str_hash, g_str_equal,
                                              free, free);

      var_node = var_node->children;
      while (true)
        {
          component_t *var;
          char *component_name;

          xml_assert_element (var_node, "var");

          component_name = xml_get_prop_string (var_node, "name");
          var = malloc (sizeof (component_t));
          *var = ecs_component_new_from_xml (var_node);

          g_hash_table_insert (var_components, component_name, var);

          XML_NEXT_OR_BREAK (var_node);
        }
      var_node = var_node->parent;
    }

  /* Construct entity and insert variable components */

  filename = g_strdup_printf ("./data/entity/%s.xml", type_name);
  doc = xml_read_file_or_exit (filename);
  node = xmlDocGetRootElement (doc);

  xml_assert_element (node, "entity");

  node = node->children;
  while (true)
    {
      component_t component;
      char *component_name;

      xml_assert_element (node, "component");

      component_name = xml_get_prop_string (node, "name");
      if (var_node && xml_has_prop (node, "var"))
        {
          assert (g_hash_table_contains (var_components, component_name));
          component = *(component_t *) g_hash_table_lookup (var_components,
                                                            component_name);
        }
      else
        component = ecs_component_new_from_xml (node);

      ecs_entity_add_component (ecs, entity, component_name, component);

      free (component_name);

      XML_NEXT_OR_BREAK (node);
    }

  if (var_node)
    g_hash_table_destroy (var_components);

  free (filename);
  xmlFreeDoc (doc);

  return entity;
}

void
ecs_entity_of_type_xml_write (ecs_t         *ecs,
                              int            entity,
                              xmlTextWriter *writer,
                              const char    *type_name)
{
  entity_type *type;

  type = g_hash_table_lookup (entity_types, type_name);
  for (int i = 0; type->vars[i]; i++)
    {
      xml_start_element (writer, "var");

      ecs_component_xml_write (ecs_entity_get_component_ptr (ecs, entity,
                                                             type->vars[i]),
                               type->vars[i], writer);

      xml_end_element (writer);
    }
}

const char **
ecs_entity_type_names (void)
{
  const char **type_names;

  type_names = (const char **) g_hash_table_get_keys_as_array (entity_types,
                                                               NULL);

  qsort (type_names, g_hash_table_size (entity_types), sizeof (const char *),
         (int (*) (const void *, const void *)) strcmp);

  return type_names;
}

int
ecs_entity_type_count (void)
{
  return g_hash_table_size (entity_types);
}

uint64_t
ecs_entity_type_signature (const char *type_name)
{
  entity_type *type;

  type = g_hash_table_lookup (entity_types, type_name);
  return type->signature;
}

const char **
ecs_entity_type_vars (const char *type_name)
{
  entity_type *type;

  type = g_hash_table_lookup (entity_types, type_name);
  return (const char **) type->vars;
}

int
ecs_entity_type_var_count (const char *type_name)
{
  const char **vars;
  int n;

  vars = ecs_entity_type_vars (type_name);
  n = 0;
  while (vars[n])
    n++;

  return n;
}

bool
ecs_entity_is_of_type (ecs_t      *ecs,
                       int         entity,
                       const char *type_name)
{
  return (ecs->signatures[entity] == ecs_entity_type_signature (type_name));
}

const char *
ecs_entity_find_type (ecs_t *ecs,
                      int    entity)
{
  const char **type_names;
  const char *result;

  result = NULL;
  type_names = (const char **) g_hash_table_get_keys_as_array (entity_types,
                                                               NULL);
  for (int i = 0; type_names[i]; i++)
    {
      if (ecs_entity_is_of_type (ecs, entity, type_names[i]))
        {
          result = type_names[i];
          break;
        }
    }

  free (type_names);

  return result;
}
