/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "components.h"

#include <assert.h>
#include <string.h>

#include "ecs.h"
#include "util/spawn-state.h"
#include "video/sprite.h"
#include "video/tilemap.h"

/* Keys are string literals, values are int pointers. */
static GHashTable *component_ids = NULL;

static void
component_ptr_copy (component_t *src,
                    component_t *dest)
{
  memcpy (dest->ptr, src->ptr, dest->size);
}

static void
component_ptr_destroy (component_t *component)
{
  free (component->ptr);
}

static void
ecs_component_register (const char *component_name)
{
  int *component_id;
  int component_count;

  component_count = g_hash_table_size (component_ids);
  assert (component_count < MAX_COMPONENTS);

  component_id = malloc (sizeof (int));
  *component_id = component_count;
  g_hash_table_insert (component_ids, (char *) component_name, component_id);
}

static void
ecs_components_register (void)
{
  ecs_component_register ("position");
  ecs_component_register ("dimensions");
  ecs_component_register ("velocity");
  ecs_component_register ("gravity");
  ecs_component_register ("collision");
  ecs_component_register ("player");
  ecs_component_register ("water");
  ecs_component_register ("checkpoint");
  ecs_component_register ("scene_warp");
  ecs_component_register ("level_end");
  ecs_component_register ("spawn_state");

  ecs_component_register ("danger");
  ecs_component_register ("puffer");

  ecs_component_register ("renderable");
  ecs_component_register ("editor_renderable");
  ecs_component_register ("Sprite");
  ecs_component_register ("Tilemap");
  ecs_component_register ("camera");
}

void
ecs_components_global_init (void)
{
  component_ids = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, free);
  ecs_components_register ();
}

void
ecs_components_global_destroy (void)
{
  g_hash_table_destroy (component_ids);
}

const char **
ecs_components_registered (void)
{
  return (const char **) g_hash_table_get_keys_as_array (component_ids, NULL);
}

void
ecs_component_copy (component_t *src,
                    component_t *dest)
{
  dest->size = src->size;
  if (!src->ptr)
    dest->ptr = NULL;
  else
    {
      dest->ptr = malloc (dest->size);
      src->copy_ptr (src, dest);
    }

  dest->copy_ptr = src->copy_ptr;
  dest->destroy_ptr = src->destroy_ptr;
}

void
ecs_component_destroy (component_t *component)
{
  if (!component->ptr)
    return;

  component->destroy_ptr (component);
}

int
ecs_component_get_id (const char *component_name)
{
  return *(int *) g_hash_table_lookup (component_ids, component_name);
}

static void *
ecs_component_init (component_t *component,
                    int          size)
{
  component->size = size;
  component->ptr = malloc (size);

  return component->ptr;
}

component_t
ecs_component_new_from_xml (const xmlNode *node)
{
  component_t component;
  char *component_name;

  component_name = xml_get_prop_string (node, "name");

  component.size = 0;
  component.ptr = NULL;
  component.copy_ptr = component_ptr_copy;
  component.destroy_ptr = component_ptr_destroy;

  if (g_str_equal (component_name, "position"))
    {
      position *pos;

      pos = ecs_component_init (&component, sizeof (position));
      pos->x = xml_get_prop_double (node, "x");
      pos->y = xml_get_prop_double (node, "y");
    }

  if (g_str_equal (component_name, "dimensions"))
    {
      dimensions *dim;

      dim = ecs_component_init (&component, sizeof (dimensions));
      dim->width = xml_get_prop_int (node, "width");
      dim->height = xml_get_prop_int (node, "height");
    }

  if (g_str_equal (component_name, "velocity"))
    {
      velocity *v;

      v = ecs_component_init (&component, sizeof (velocity));
      v->horizontal = xml_get_prop_double (node, "horizontal");
      v->vertical = xml_get_prop_double (node, "vertical");
      v->max_horizontal = xml_get_prop_double (node, "max_horizontal");
      v->max_vertical = xml_get_prop_double (node, "max_vertical");
    }

  if (g_str_equal (component_name, "gravity"))
    {
      gravity *g;

      g = ecs_component_init (&component, sizeof (gravity));
      g->acceleration = xml_get_prop_double (node, "acceleration");
    }

  if (g_str_equal (component_name, "collision"))
    {
      collision *col;

      col = ecs_component_init (&component, sizeof (collision));
      col->up = false;
      col->down = false;
      col->left = false;
      col->right = false;
    }

  if (g_str_equal (component_name, "Tilemap"))
    {
      Tilemap *tilemap;

      tilemap = ecs_component_init (&component, sizeof (Tilemap));
      tilemap->tileset = xml_get_prop_string (node, "tileset");
      tilemap->tiles_length = 0;

      component.copy_ptr = tilemap_ptr_copy;
      component.destroy_ptr = tilemap_ptr_destroy;
    }

  if (g_str_equal (component_name, "checkpoint"))
    {
      checkpoint *cp;

      cp = ecs_component_init (&component, sizeof (checkpoint));
      cp->activated = false;
    }

  if (g_str_equal (component_name, "puffer"))
    {
      puffer *puff;

      puff = ecs_component_init (&component, sizeof (puffer));
      puff->periods_elapsed = xml_get_prop_int (node, "periods_elapsed");
      puff->movement_direction = xml_get_prop_int (node, "movement_direction");
      puff->accelerate = xml_get_prop_bool (node, "accelerate");
    }

  if (g_str_equal (component_name, "Sprite"))
    {
      Sprite *sprite;

      sprite = ecs_component_init (&component, sizeof (Sprite));
      sprite->dest = (SDL_Rect) { 0, 0, 0 ,0 };
      sprite->type = STATIC_SPRITE;
      sprite->texture = NULL;
      sprite->animation = NULL;
      timer_init (&sprite->animation_timer);
    }

  if (g_str_equal (component_name, "scene_warp"))
    {
      scene_warp *sw;

      sw = ecs_component_init (&component, sizeof (scene_warp));
      sw->scene_index = xml_get_prop_int (node, "scene_index");
      sw->player_x = xml_get_prop_int (node, "player_x");
      sw->player_y = xml_get_prop_int (node, "player_y");
    }

  if (g_str_equal (component_name, "spawn_state"))
    {
      spawn_state *ss;

      ss = ecs_component_init (&component, sizeof (spawn_state));

      for (int i = 0; i < MAX_COMPONENTS; i++)
        {
          ss->components[i].ptr = NULL;
          ss->components[i].size = 0;
        }

      component.copy_ptr = spawn_state_ptr_copy;
      component.destroy_ptr = spawn_state_ptr_destroy;
    }

  if (g_str_equal (component_name, "player"))
    {
      player *plr;

      plr = ecs_component_init (&component, sizeof (player));
      timer_init (&plr->wall_jump_slow_timer);
      plr->can_wall_jump_from = 0;
    }

  if (g_str_equal (component_name, "renderable"))
    {
      renderable *rend;

      rend = ecs_component_init (&component, sizeof (renderable));
      rend->layer = xml_get_prop_int (node, "layer");
    }

  if (g_str_equal (component_name, "editor_renderable"))
    {
      editor_renderable *erend;

      erend = ecs_component_init (&component, sizeof (editor_renderable));
      erend->layer = xml_get_prop_int (node, "layer");
    }

  if (g_str_equal (component_name, "camera"))
    {
      camera *cam;

      cam = ecs_component_init (&component, sizeof (camera));
      cam->center_x = xml_get_prop_double (node, "center_x");
      cam->center_y = xml_get_prop_double (node, "center_y");
      cam->zoom = xml_get_prop_double (node, "zoom");
      cam->threshold = xml_get_prop_double (node, "threshold");
    }

  free (component_name);

  return component;
}

void
ecs_component_xml_write (void          *component_ptr,
                         const char    *component_name,
                         xmlTextWriter *writer)
{
  xml_write_attribute (writer, "name", component_name);

  if (g_str_equal (component_name, "position"))
    {
      position *pos;

      pos = component_ptr;
      xml_write_attribute_double (writer, "x", pos->x);
      xml_write_attribute_double (writer, "y", pos->y);
    }

  if (g_str_equal (component_name, "dimensions"))
    {
      dimensions *dim;

      dim = component_ptr;
      xml_write_attribute_int (writer, "width", dim->width);
      xml_write_attribute_int (writer, "height", dim->height);
    }

  if (g_str_equal (component_name, "velocity"))
    {
      velocity *v;

      v = component_ptr;
      xml_write_attribute_double (writer, "horizontal", 0);
      xml_write_attribute_double (writer, "vertical", 0);
      xml_write_attribute_double (writer, "max_horizontal", v->max_horizontal);
      xml_write_attribute_double (writer, "max_vertical", v->max_vertical);
    }

  if (g_str_equal (component_name, "gravity"))
    {
      gravity *g;

      g = component_ptr;
      xml_write_attribute_double (writer, "acceleration", g->acceleration);
    }

  if (g_str_equal (component_name, "Tilemap"))
    {
      Tilemap *tilemap;

      tilemap = component_ptr;
      xml_write_attribute (writer, "tileset", tilemap->tileset);
    }

  if (g_str_equal (component_name, "puffer"))
    {
      puffer *puff;

      puff = component_ptr;
      xml_write_attribute_int (writer, "periods_elapsed",
                               puff->periods_elapsed);
      xml_write_attribute_int (writer, "movement_direction",
                               puff->movement_direction);
      xml_write_attribute_bool (writer, "accelerate",
                                puff->accelerate);
    }

  if (g_str_equal (component_name, "scene_warp"))
    {
      scene_warp *sw;

      sw = component_ptr;
      xml_write_attribute_int (writer, "scene_index", sw->scene_index);
      xml_write_attribute_int (writer, "player_x", sw->player_x);
      xml_write_attribute_int (writer, "player_y", sw->player_y);
    }

  if (g_str_equal (component_name, "renderable"))
    {
      renderable *rend;

      rend = component_ptr;
      xml_write_attribute_int (writer, "layer", rend->layer);
    }

  if (g_str_equal (component_name, "editor_renderable"))
    {
      editor_renderable *erend;

      erend = component_ptr;
      xml_write_attribute_int (writer, "layer", erend->layer);
    }

  if (g_str_equal (component_name, "camera"))
    {
      camera *cam;

      cam = component_ptr;
      xml_write_attribute_double (writer, "center_x", cam->center_x);
      xml_write_attribute_double (writer, "center_y", cam->center_y);
      xml_write_attribute_double (writer, "zoom", cam->zoom);
      xml_write_attribute_double (writer, "threshold", cam->threshold);
    }
}
