/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ecs.h"

void
ecs_global_init (void)
{
  ecs_components_global_init ();
  ecs_entity_types_global_init ();
  ecs_signatures_calculate ();
}

void
ecs_global_destroy (void)
{
  ecs_entity_types_global_destroy ();
  ecs_components_global_destroy ();
}

void
ecs_init (ecs_t *ecs)
{
  ecs->entity_count = 0;
}

void
ecs_destroy (ecs_t *ecs)
{
  while (ecs->entity_count > 0)
    ecs_entity_delete (ecs, 0);
}

void
ecs_copy (ecs_t *src,
          ecs_t *dest)
{
  /* Copy entities */
  dest->entity_count = src->entity_count;
  for (int entity = 0; entity < dest->entity_count; entity++)
    {
      const char **component_names;

      dest->signatures[entity] = src->signatures[entity];

      /* Copy components of entity */
      component_names = ecs_entity_get_component_names (src, entity);
      for (int i = 0; component_names[i]; i++)
        {
          int component_id;

          component_id = ecs_component_get_id (component_names[i]);
          ecs_component_copy (&src->components[component_id][entity],
                              &dest->components[component_id][entity]);
        }

      free (component_names);
    }
}
