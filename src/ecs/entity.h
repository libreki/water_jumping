/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ENTITY_H
#define ENTITY_H

#include <stdint.h>

#include "components.h"
#include "decl.h"
#include "util/xml.h"

/* Returns ID of new entity. */
int           ecs_entity_new                 (ecs_t *ecs);
void          ecs_entity_new_from_xml        (ecs_t   *ecs,
                                              xmlNode *node);
void          ecs_entity_delete              (ecs_t *ecs,
                                              int    entity);

void          ecs_entity_xml_write           (ecs_t         *ecs,
                                              int            entity,
                                              xmlTextWriter *writer);

void          ecs_entity_add_component       (ecs_t       *ecs,
                                              int          entity,
                                              const char  *component_name,
                                              component_t  component);
/* Returns NULL terminated array of strings. Caller is responsible for freeing
   array but not strings. */
const char  **ecs_entity_get_component_names (ecs_t *ecs,
                                              int    entity);
component_t  *ecs_entity_get_component       (ecs_t      *ecs,
                                              int         entity,
                                              const char *component_name);
void         *ecs_entity_get_component_ptr   (ecs_t      *ecs,
                                              int         entity,
                                              const char *component_name);
bool          ecs_entity_has_component       (ecs_t      *ecs,
                                              int         entity,
                                              const char *component_name);

/* Returns -1 terminated array of entity IDs. Caller is responsible for freeing
   array. */
int          *ecs_entities_find              (ecs_t    *ecs,
                                              uint64_t  signature);

#endif
