/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "entity.h"

#include <assert.h>

#include "ecs.h"

int
ecs_entity_new (ecs_t *ecs)
{
  int entity;

  assert (ecs->entity_count <= MAX_ENTITIES);

  entity = ecs->entity_count;
  ecs->signatures[entity] = 0;

  ecs->entity_count++;

  return entity;
}

void
ecs_entity_new_from_xml (ecs_t   *ecs,
                         xmlNode *node)
{
  if (xml_has_prop (node, "type"))
    {
      char *entity_type;

      entity_type = xml_get_prop_string (node, "type");
      ecs_entity_of_type_new (ecs, entity_type, node);
      free (entity_type);
    }
  else
    {
      int entity;

      entity = ecs_entity_new (ecs);

      node = node->children;
      while (true)
        {
          component_t component;
          char *component_name;

          xml_assert_element (node, "component");

          component_name = xml_get_prop_string (node, "name");
          component = ecs_component_new_from_xml (node);

          ecs_entity_add_component (ecs, entity, component_name, component);

          free (component_name);

          XML_NEXT_OR_BREAK (node);
        }
      node = node->parent;
    }
}

/* Deletes entity and moves final_entity to take its place */
void
ecs_entity_delete (ecs_t *ecs,
                   int    entity)
{
  const char **component_names;
  int final_entity;

  final_entity = ecs->entity_count - 1;
  component_names = ecs_components_registered ();
  for (int i = 0; component_names[i]; i++)
    {
      int component_id;

      component_id = ecs_component_get_id (component_names[i]);

      if (ecs_entity_has_component (ecs, entity, component_names[i]))
        {
          ecs_component_destroy (&ecs->components[component_id][entity]);
        }

      if (ecs_entity_has_component (ecs, final_entity, component_names[i]))
        {
          ecs->components[component_id][entity] =
            ecs->components[component_id][final_entity];
        }
    }

  free (component_names);

  ecs->signatures[entity] = ecs->signatures[final_entity];
  ecs->entity_count--;
}

void
ecs_entity_xml_write (ecs_t         *ecs,
                      int            entity,
                      xmlTextWriter *writer)
{
  const char *type_name;
  const char **component_names;

  type_name = ecs_entity_find_type (ecs, entity);
  if (type_name)
    {
      xml_write_attribute (writer, "type", type_name);
      ecs_entity_of_type_xml_write (ecs, entity, writer, type_name);
      return;
    }

  component_names = ecs_entity_get_component_names (ecs, entity);
  for (int i = 0; component_names[i]; i++)
    {
      xml_start_element (writer, "component");

      ecs_component_xml_write
        (ecs_entity_get_component_ptr (ecs, entity, component_names[i]),
         component_names[i], writer);

      xml_end_element (writer);
    }

  free (component_names);
}

void
ecs_entity_add_component (ecs_t       *ecs,
                          int          entity,
                          const char  *component_name,
                          component_t  component)
{
  int component_id;

  component_id = ecs_component_get_id (component_name);

  ecs->components[component_id][entity] = component;
  ecs->signatures[entity] |= 1 << component_id;
}

const char **
ecs_entity_get_component_names (ecs_t *ecs,
                                int    entity)
{
  const char **all;
  const char **has;
  int i;

  all = ecs_components_registered ();
  has = malloc ((MAX_COMPONENTS + 1) * sizeof (const char *));

  i = 0;
  for (int j = 0; all[j]; j++)
    {
      if (ecs_entity_has_component (ecs, entity, all[j]))
        {
          has[i] = all[j];
          i++;
        }
    }

  free (all);

  has[i] = NULL;
  return has;
}

component_t *
ecs_entity_get_component (ecs_t      *ecs,
                          int         entity,
                          const char *component_name)
{
  if (!ecs_entity_has_component (ecs, entity, component_name))
    {
      fprintf (stderr, "Failed to get component %s from entity %d.\n",
               component_name, entity);
      exit (1);
    }

  return &ecs->components[ecs_component_get_id (component_name)][entity];
}

void *
ecs_entity_get_component_ptr (ecs_t      *ecs,
                              int         entity,
                              const char *component_name)
{
  return ecs_entity_get_component (ecs, entity, component_name)->ptr;
}

bool
ecs_entity_has_component (ecs_t      *ecs,
                          int         entity,
                          const char *component_name)
{
  return ((ecs->signatures[entity]
           & (1 << ecs_component_get_id (component_name)))
          != 0);
}

int *
ecs_entities_find (ecs_t    *ecs,
                   uint64_t  signature)
{
  int *entities;
  int length;

  entities = malloc ((MAX_ENTITIES + 1) * sizeof (int));
  length = 0;
  for (int entity = 0; entity < ecs->entity_count; entity++)
    {
      if ((ecs->signatures[entity] & signature) == signature)
        {
          entities[length] = entity;
          length++;
        }
    }

  entities[length] = -1;
  return entities;
}
