/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef COMPONENTS_H
#define COMPONENTS_H

#include "decl.h"
#include "util/timer.h"
#include "util/xml.h"

typedef struct component_t component_t;

struct component_t
{
  void *ptr;
  int   size;

  void (*copy_ptr)    (component_t *src,
                       component_t *dest);
  void (*destroy_ptr) (component_t *component);
};

typedef struct
{
  double x;
  double y;
} position;

typedef struct
{
  int width;
  int height;
} dimensions;

typedef struct
{
  double horizontal;
  double vertical;
  double max_horizontal;
  double max_vertical;
} velocity;

typedef struct
{
  double acceleration;
} gravity;

typedef struct
{
  bool up;
  bool down;
  bool left;
  bool right;
} collision;

typedef struct
{
  bool activated;
} checkpoint;

typedef struct
{
  int  periods_elapsed;
  int  movement_direction;
  bool accelerate;
} puffer;

typedef struct
{
  int    scene_index;
  double player_x;
  double player_y;
} scene_warp;

typedef struct
{
  component_t components[MAX_COMPONENTS];
} spawn_state;

typedef struct
{
  /* After wall jumping, the player's movement capabilities are limited for a
   * short time. */
  timer wall_jump_slow_timer;

  /* -1 for left, 1 for right, 0 for nowhere. */
  int   can_wall_jump_from;
} player;

typedef struct
{
  int layer;
} renderable, editor_renderable;

typedef struct camera
{
  double center_x;
  double center_y;

  /* Zoom value applied to view with default size matching window size. Default
   * value is 3, same as the standard game zoom. */
  double zoom;

  /* Value between 0 and 1. The distance the player needs to be from the center
   * of a camera entity for the camera lock to be in full effect. Expressed as
   * a percentage from the center of the camera entity to its horizontal or
   * vertical edges with 1 being the entire camera entity. */
  double threshold;
} camera;

void           ecs_components_global_init    (void);
void           ecs_components_global_destroy (void);

/* Returns NULL-terminated array of strings. Caller is responsible for freeing
 * array but not strings. */
const char   **ecs_components_registered     (void);

void           ecs_component_copy            (component_t *src,
                                              component_t *dest);
void           ecs_component_destroy         (component_t *component);

int            ecs_component_get_id          (const char *component_name);

component_t    ecs_component_new_from_xml    (const xmlNode *node);
void           ecs_component_xml_write       (void          *component_ptr,
                                              const char    *component_name,
                                              xmlTextWriter *writer);

#endif
