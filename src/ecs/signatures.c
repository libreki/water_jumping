/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "signatures.h"

#include <assert.h>

#include <glib.h>

#include "ecs.h"

SIGNATURES (uint64_t);

static uint64_t
ecs_signature_calculate (int n, ...)
{
  uint64_t signature;
  va_list ap;

  assert (n < MAX_COMPONENTS);

  signature = 0;
  va_start (ap, n);
  for (int i = 0; i < n; i++)
    signature |= 1 << ecs_component_get_id (va_arg (ap, const char *));

  va_end (ap);

  return signature;
}

void
ecs_signatures_calculate (void)
{
  movable_sig = ecs_signature_calculate (2,
                                         "position",
                                         "velocity");

  gravity_sig = ecs_signature_calculate (2,
                                         "gravity",
                                         "velocity");

  collidable_sig = ecs_signature_calculate (4,
                                            "position",
                                            "dimensions",
                                            "velocity",
                                            "collision");
  obstacle_sig = ecs_signature_calculate (3,
                                          "position",
                                          "dimensions",
                                          "collision");

  renderable_sig = ecs_signature_calculate (3,
                                            "renderable",
                                            "position",
                                            "dimensions");

  tilemap_sig = ecs_signature_calculate (3,
                                         "position",
                                         "dimensions",
                                         "Tilemap");

  rect_sig = ecs_signature_calculate (2,
                                      "position",
                                      "dimensions");

  danger_sig = ecs_signature_calculate (3,
                                        "position",
                                        "dimensions",
                                        "danger");

  spawn_state_sig = ecs_signature_calculate (1,
                                             "spawn_state");

  editor_renderable_sig = ecs_signature_calculate (3,
                                                   "editor_renderable",
                                                   "position",
                                                   "dimensions");
}
