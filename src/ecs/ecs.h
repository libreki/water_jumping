/*  Copyright (C) 2022-2023 libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ESC_H
#define ESC_H

#include <stdint.h>

#include <glib.h>

#include "components.h"
#include "entity-type.h"
#include "entity.h"
#include "signatures.h"

struct ecs_t
{
  int         entity_count;

  component_t components[MAX_COMPONENTS][MAX_ENTITIES];
  uint64_t    signatures[MAX_ENTITIES];
};

void ecs_global_init    (void);
void ecs_global_destroy (void);

void ecs_init           (ecs_t *ecs);
void ecs_destroy        (ecs_t *ecs);
void ecs_copy           (ecs_t *src,
                         ecs_t *dest);

#endif
