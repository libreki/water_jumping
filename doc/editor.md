<!--
SPDX-FileCopyrightText: 2022-2023 libreki

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Editor instructions

> This file is licensed under the Creative Commons Attribution-ShareAlike 4.0
> International License.

## Using the editor

The editor can be entered through the main menu by selecting the "Editor"
option.

While in the editor, there will be text in the top left corner saying `EDITOR`.
When the text is yellow, editing functionality is enabled, and when it is
white, the game systems update normally to test the level. Editing
functionality can be toggled on and off by pressing F4.

## Saving and loading levels

After selecting "Editor" from the main menu, you have to enter the name of the
level you want to edit or the name of a new level to create. Levels are stored
as XML documents in `data/level`, with the name of the level being its filename
without the `.xml` extension. For example, the name of the level stored in
`data/level/foo.xml` is `foo`.

To save the edited level to a file, press `F12` while in the editor. The level
will be saved in `data/level` with the same name that was entered when starting
the editor, either overwriting the existing level file or creating a new one.

## Moving view

The view in the editor can be moved by holding down the left mouse button and
dragging the view with the mouse cursor.

The view can be zoomed in and out using the mouse scroll wheel.

## Adding and removing entities

Entities can be removed by selecting them and pressing the delete key.

To create entities, press `E` to open a menu listing entity types, press the
jump button to select an entity type you wish to create. Then press `R` to
create an entity of the selected type at the position of the mouse cursor.

## Selecting and manipulating entities

Entities can be selected by clicking on them with the left mouse button.

When an entity is selected, press `C` to open a menu listing the variable
components of that entity. To edit a particular component, select it from the
menu, this will open a submenu listing all the editable properties of that
component. Select the property you want to edit, this opens a text entry where
you can manually type in a new value, press enter to submit the value.

Press the pause button to exit out of the menus.

### Moving entities

Selected entities can be moved in three ways:

 - Edit the `position` component as described before.

 - Hold the right mouse button and move the entity using the mouse cursor.

 - Press the arrow keys to move the entity in a 128x128 unit grid.

### Resizing entities

Selected entities can be resized in two ways:

 - Edit the `dimensions` component as described before.

 - While left shift is held, press the arrow keys to increase or decrease the
   width or height of the entity by 128 units.

## Key bind reference

 - **F4**: Toggle editing functionality
 - **F12**: Save level to file
 - **Up arrow**: Move object up
 - **Left arrow**: Move object left
 - **Down arrow**: Move object down
 - **Right arrow**: Move object right
 - **Up arrow (left shift held)**: Decrease height
 - **Left arrow (left shift held)**: Decrease width
 - **Down arrow (left shift held)**: Increase height
 - **Right arrow (left shift held)**: Increase width
 - **E**: Open entity menu
 - **C**: Open component menu for selected entity
 - **R**: Create entity
 - **Delete**: Delete selected entity
