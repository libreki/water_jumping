#  Copyright (C) 2022-2023 libreki
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  SPDX-License-Identifier: GPL-3.0-or-later

CFLAGS   = -std=c99 -g -Wall -Wextra -Isrc
CPPFLAGS = -c

SRCDIR   = src
SRCS     = $(shell find $(SRCDIR) -name '*.c')

LIBS     = glib-2.0 libxml-2.0 sdl2 SDL2_ttf SDL2_image

ifeq ($(MAKECMDGOALS), windows)
	BIN       = main.exe
	BUILDDIR  = build/windows
	CC        = x86_64-w64-mingw32-gcc

	GLIB      = lib/glib/build/glib/libglib-2.0-0.dll
	LIBXML2   = lib/libxml2/.libs/libxml2-2.dll
else
	BIN       = main.out
	BUILDDIR  = build/gnu-linux
	CFLAGS   += $(shell pkg-config --cflags $(LIBS))
	LDLIBS    = $(shell pkg-config --libs $(LIBS)) -lm
endif

OBJDIR = $(BUILDDIR)/obj
OBJS   = $(patsubst %.c, $(OBJDIR)/%.o, $(SRCS))

.PHONY: all run valgrind valgrindflag clean
all: $(BUILDDIR)/$(BIN)

$(BUILDDIR)/$(BIN): $(OBJS)
	$(CC) $^ -o $@ $(LDLIBS)

$(OBJDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CPPFLAGS) $< -o $@

run: $(BUILDDIR)/$(BIN)
	$(BUILDDIR)/$(BIN)

$(GLIB):
	cd lib/glib \
	&& meson setup --cross-file ../glib_cross_file.txt build \
	&& meson compile -C build

$(LIBXML2):
	export CC=$(CC) \
	&& cd lib/libxml2 \
	&& ./autogen.sh \
	&& ./configure --build=mingw64 --without-zlib --without-lzma \
	   --without-python --without-threads \
	&& $(MAKE)

windows: $(GLIB) $(LIBXML2)

clean:
	rm -rf build
