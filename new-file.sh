#!/bin/bash
#
#  Copyright (C) 2022-2023 libreki
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  SPDX-License-Identifier: GPL-3.0-or-later

if [ -z "$1" ]; then
    echo "No argument given"
    exit 1
fi

# REUSE-IgnoreStart
LICENSE="/*  Copyright (C) $(date +%Y) libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */
"
# REUSE-IgnoreEnd

HEADER="#ifndef _H
#define _H



#endif"

SOURCE="#include \"$1.h\""

echo "$LICENSE" >> src/$1.c
echo "$SOURCE" >> src/$1.c

echo "$LICENSE" >> src/$1.h
echo "$HEADER" >> src/$1.h
